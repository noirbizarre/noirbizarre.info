Java et proxy HTTP
##################

:date: 2011-04-06 09:22
:author: noirbizarre
:category: Java
:tags: http, java, proxy
:lang: fr

Comment faire en sorte qu'une application Java passe par un proxy HTTP
(comme c'est souvent le cas en entreprise) ?

Il faut le spécifier dans les options de la JVM à l'execution!

On me pose souvent la question et même si je connais la réponse, je doit
toujours la rechercher pour être sur de la syntaxe.

Les options à passer sont:

-  ``http.proxyHost`` pour spécifier l'hôte
-  ``http.proxyPort`` pour spécifier le port
-  ``http.proxyUser`` pour spécifier le nom d'utilisateur (optionnel)
-  ``http.proxyPassword`` pour spécifier le mot de passe (optionnel)


Ce qui donnerai en ligne de commande pour un proxy dont l'url est
http://user:password@proxy.maboite.com:8080

.. code-block:: bash

    ~$ java -Dhttp.proxyHost=proxy.maboite.com -Dhttp.proxyPort=8080 -Dhttp.proxyUser=user -Dhttp.proxyPassword=password maClasse
