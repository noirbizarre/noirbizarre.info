Définir l’exécution par défaut d'un plugin maven
################################################
:date: 2011-10-03 16:41
:author: noirbizarre
:category: Java
:tags: maven
:lang: fr

Avec Maven, il arrive souvent d'avoir à définir plusieurs exécutions
d'un plugin pour différentes phases mais de vouloir en garder une par
défaut (celle utilisée en ligne de commande).

Depuis la version 2.2 de maven, le ticket `MNG-3401`_ le permet en
donnant l'identifiant d’exécution ``default-cli``:

.. code-block:: xml

    <plugin>
        <artifactId>maven-assembly-plugin</artifactId>
        <executions>
            <execution>
                <id>default-cli</id>
                <configuration>
                    <descriptorRefs>
                        <descriptorRef>jar-with-dependencies</descriptorRef>
                        <descriptorRef>project</descriptorRef>
                    </descriptorRefs>
                </configuration>
            </execution>
        </executions>
    </plugin>


.. _MNG-3401: http://jira.codehaus.org/browse/MNG-3401
