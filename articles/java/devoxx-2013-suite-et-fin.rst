Devoxx 2013 - Suite et fin
##########################

:date: 2013-05-28
:author: noirbizarre
:category: Java
:tags: java, devoxx, tests, valtech, speaker
:lang: fr

Voilà un article qui arrive un peu tard, mais des imprévus ont détournés mon attention
et je n'ai jamais publié les articles sur les deuxième et troisième jours à `Devoxx France 2013`_.

Voici donc mon rattrappage, basé sur quelques notes et mes souvenirs, qui se découpe comme suit:

- Notre présentation
- Les présentations que j'ai vues sur ces 2 derniers jours
- Les goodies
- Conclusion


Notre présentation: `Les tests: pourquoi, comment ?`_
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Comme je le disais dans l'`article précédant`_, cette année je suis speaker avec deux collègues: `Gregory Paul`_ et `Xavier Renaudin`_.

Ce talk d'une heure sur le thème des tests n'a pas été forcement simple à mettre en place,
principalement pour deux raisons:

- s'organiser à trois sur un talk d'une heure n'est pas évident
- le sujet est vaste et peu revétir plusieurs aspests

Effectivement s'organiser à trois sur un talk d'une heure signifie qu'il faut être extrement rigoureux sur la gestion du temps.
Cela signifie aussi qu'il faut pouvoir trouver des créneaux de répétition où nous  étions tous les trois disponibles.
Heureusement `Valtech`_ a pu nous dégager du temps et comme nous sommes tous les 3 en mission chez `Mappy`_, nous avons pu passer certains midis à répeter sandwich à la main.

Concernant le sujet, le challenge pour nous a été:

    Comment traiter ce sujet en ayant le bon niveau de technicité ?

Ayant trois profils différents sur une même mission,
nous avons choisi une approche par l'expérience qui nous permis de balayer le sujet
de l'approche la plus humaine et fonctionnelle possible jusqu'à un niveau bien plus technique.

Cette approche nous à permis de confronter nos visions, et, il faut le dire,
c'est surement l'aspect de le plus enrichisant de cette expérience.

Mais revenons à ce deuxième jour, jour de notre présentation!

.. image:: |filename|/images/devoxx/nous-sur-scene.jpg
    :alt: Nous sur scène
    :height: 500px
    :align: center


L'expérience a commencé bien avant le talk, entre la speaker room (cf. `article précédant`_),
les échanges sur le salon et biensur `Twitter`_.

Finalement, malgré la pression et le stress, tout s'est bien passé.
La gestion du chrono a été très juste et nous n'avons eu le temps de répondre qu'à une seule question.
Cela ne nous a pas empêché de discuter avec les personnes intéréssées après la présentation.

Pour ma part, je pense que l'expérience sera reconduite aussi souvent que possible.

La présentation n'est pas encore disponible sur `Parleys`_ mais vous pouvez déjà visionner les premières présentation sur le `channel Devoxx France 2013`_.
Je mettrais à jour cet article quand elle sera disponible.
En attendant, vous pouvez consulter `les slides de la présentation`_ et `le code source des démos`_.

Vous pouvez aussi consulter l'`article de Valtech`_ sur le sujet.


Les présentations auxquelles j'ai assisté
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Voici un petit résumé des présentations auxquelles j'ai assité les deux derniers jours.

`Reality is graph`_
-------------------

Lors de ce talk, "(REALITY)-[:IS_A]->(GRAPH)", `Florent Biville`_ nous a présenté `Neo4j`_,
une base de données orientée graph, avec une approche par le code.

`BFS`_, `DFS`_ et autres algorithmes nous ont été présentés sur `Neo4j`_
ainsi que `Cypher`_, le language de requêtage.

`Neo4j`_ dispose aussi d'un API REST extensible et de bindings pour différents languages.

L'approche graphe me semble très intéréssante et originale,
je vais me renseigner un peu plus sur le sujet.


`Comparing JVM Web Frameworks`_
-------------------------------

Je n'ai qu'une chose à dire: "What a speaker !!" `Matt Raible`_ est impressionant!
Sa présentation est fluide, rythmée de petites touches d'humour qui font mouche dans le public.

Le sujet est lui aussi très intéréssant: comment comparer des frameworks.
Sous le pretexte d'un comparatif des frameworks MVC,
Matt nous démontre qu'il n'y a pas de méthode universelle pour le faire.
Conclusion: comparez vous-même les frameworks qui correspondent à vos besoins!

Nous passons juste après cette présentation, rien de mieux pour faire monter la pression.

Vous pouvez lire `l'article de Matt sur sa propre présentation`_.


`Real Options: Quand et comment (ne pas) prendre des decisions`_
----------------------------------------------------------------

`Pascal Van Cauwenberghe`_ nous a présenté quelques techniques de décision
et surtout comment faire en sorte de les prendre au plus tard.

Une très bonne conférence non technique, très bien présentée,
sur ton d'humour avec un accent belge raffraichissant !


Les goodies
~~~~~~~~~~~

Que serait un salon sans goodies. Devoxx France ne fait pas exception à la règle.

.. image:: |filename|/images/devoxx/goodies.jpg
    :alt: Les goodies 2013
    :height: 500px
    :align: center

Au programme cette année:

- T-Shirts
- Balles anti-stress
- Tapis de souris
- Accessoires d'agiliste
- Mugs, Gobelets...
- Bonbons
- Sac-à-dos
- Livres
- Jeux de cartes
- ...

Du classique direz-vous. Oui, mais ça fait toujours plaisir!


Conclusion
~~~~~~~~~~

Devoxx France 2013 fut une très bonne expérience, que ce soit en tant que spectateur ou speaker.
La tendance du développement web "client-side" a été clairement marquée.
Que dire de plus si ce n'est: vivement l'année prochaine!!!


.. _Devoxx France 2013: http://www.devoxx.com
.. _la présentation: http://www.devoxx.com/display/FR13/Les+tests++pourquoi+et+comment
.. _article précédant: |filename|devoxx-2013-jour-1.rst

.. _`Les tests: pourquoi, comment ?`: http://www.devoxx.com/display/FR13/Les+tests++pourquoi+et+comment
.. _Gregory Paul: https://twitter.com/paulgreg
.. _Xavier Renaudin: http://www.devoxx.com/display/FR13/Xavier+Renaudin
.. _Parleys: http://www.parleys.com/
.. _channel Devoxx France 2013: http://www.parleys.com/channel/516409b4e4b082c6506c9e3a
.. _les slides de la présentation: http://valtechtechno.github.io/devoxx-2013-tests-pourquoi-comment/slides/
.. _le code source des démos: https://github.com/ValtechTechno/devoxx-2013-tests-pourquoi-comment
.. _article de Valtech: http://blog.valtech.fr/2013/04/09/devoxx-france-2013/

.. _Reality is a graph: http://www.devoxx.com/display/FR13/RealityIS_AGraph
.. _Florent Biville: https://twitter.com/fbiville
.. _Neo4j: http://www.neo4j.org/
.. _Cypher: http://www.neo4j.org/learn/cypher
.. _BFS: http://fr.wikipedia.org/wiki/Breadth_First_Search
.. _DFS: http://fr.wikipedia.org/wiki/Algorithme_de_parcours_en_profondeur

.. _Comparing JVM Web Frameworks: http://www.devoxx.com/display/FR13/Comparing+JVM+Web+Frameworks
.. _`l'article de Matt sur sa propre présentation`: http://raibledesigns.com/rd/entry/devoxx_france_a_great_conference
.. _Matt Raible: http://twitter.com/mraible

.. _`Real Options: Quand et comment (ne pas) prendre des decisions`: http://www.devoxx.com/display/FR13/Real+Options++quand+et+comment+%28ne+pas%29+prendre+des+decisions
.. _`Pascal Van Cauwenberghe`: https://twitter.com/pascalvc

.. _Twitter: https://twitter.com
.. _Valtech: http://www.valtech.fr
.. _Mappy: http://www.mappy.fr
