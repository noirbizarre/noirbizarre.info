Devoxx 2013 - Jour 1
####################

:date: 2013-04-27
:author: noirbizarre
:category: Java
:tags: http, java, devoxx, javascript, tests, valtech
:lang: fr

Ça y est, c'est parti pour 3 jours de salon `Devoxx France 2013`_, 3 jours de:

- conférences
- hands-on
- rencontres et débats
- découverte
- goodies
- ...

Bref 3 journées enrichissantes pour qui s'intéresse à l'univers de l'informatique,
en particulier du développement et de l'architecture Java/Javascript !

Mais, `Devoxx France 2013`_ c'est aussi:

- 1 journée Université, 2 journées Conférence
- environs 130 présentations
- 75% des présentations en français, 25% en anglais
- 6 grandes salles de conférence d'une capacité d'environ 250 personnes
- 4 petites salles pour les ateliers, BOFs et quickies
- 660 m² de hall d’exposition
- entre 1100 et 1400 personnes attendues du 27 au 29 mars 2013


Cette année est un peu particulière parce que je suis officiellement speaker !

.. image:: |filename|/images/devoxx/pass-speaker.jpg
    :alt: Mon pass speaker
    :height: 500px
    :align: center

La speaker room a d'ailleurs été une expérience très intéressante, mais j'en parlerai plus tard.

Cette première journée en tant que spectateur a été plus orientée JavaScript que pas Java pour moi.
Ça tombe parfaitement, c'est un sujet qui m’intéresse particulièrement en ce moment..

Cette première journée est sur le format Université, c'est à dire 3h de présentation
entrecoupée d'une pause pour se rafraîchir.

.. image:: |filename|/images/devoxx/buffet.jpg
    :alt: Mon pass speaker
    :height: 400px
    :align: center


Ce format permet de rentrer en détail dans les sujets, même si, il faut l'avouer,
il n'est pas évident de rester concentré tout ce temps.


`Le fantôme, le zombie et le testacular`_
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Une très bonne présentation au format Université par `Jean-Laurent de Morlhon`_ et `Pierre Gayvallet`_
sur les tests d'applications web "modernes".

Au programme de cette présentation, entre Javascript et CoffeeScript:

- Présentation de `PhantomJS`_
- Présentation et démo de `CasperJS`_
- Présentation et démo de `Zombie.js`_
- Présentation et démo de `Testacular`_
- Présentation de `Sinon.js`_

Les démos en live-coding ont montrées la puissance de ces outils.

J'ai particulièrement apprécié l'introduction sur les tests,
introduction qui touche au cœur de notre présentation.


`Frontend Live Coding`_
~~~~~~~~~~~~~~~~~~~~~~~

Toujours dans la continuité de l'outillage pour le développement client/front,
cette Université présente une stack complète d'outils pour le développeur.

Lors de cette conférence de 3 heures de live-coding sans slides,
`Frédric Camblor`_ a effectué un tour d'horizon de différents outils qu'il utilise
et nous délivre ses astuces et best-practices associées:

- `Yeoman`_, un outil de bootstrap d'application
- `Grunt`_, le Makefile du javascript
- `Sass/Compass`_, un superset de CSS 3, équivalent de `LessCss`_.
- `Bower`_, un gestionnaire de packages avec gestion des dépendances transitives
- `Require.js`_, gestion des dépendances et modularisation du code Javascript
- `Backbone.js`_, un framework MVC pour Javascript
- `Handlebar.js`_, un moteur de template côté client
- `Rivets.js`_, un moteur de bindings bi-directionnels


`Space Chatons: Bleeding Edge HTML5`_
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

J'ai participé en tant que joueur à cette présentation délirante sur HTML5
par `Philippe Antoine`_ et `Pierre Gayvallet`_.

Avec comme support un jeu de recherche de chats,
nous avons parcouru certaines fonctionnalités récentes
et avancées de HTML5/CSS3/Javascript:

- `Backbone.js`_
- Webworkers
- websockets
- CSS 3: transforms, keyframes, 3D...
- API Text Speech Synthesis
- API Webcamet Microphone
- WebRTC
- localStorage
- drag & drop
- ...

Une excellente présentation dynamique sur ton d'humour.


La speaker room
~~~~~~~~~~~~~~~

Étant speaker cette année, j'ai passé un peu de temps dans la speaker room !

.. image:: |filename|/images/devoxx/speaker-room.jpg
    :alt: La speaker room
    :height: 400px
    :align: center

J'ai eu la joie de pouvoir participer à des discussions théoriques
et des débats enflammés avec d'autres speakers!
Franchement, ça fait plaisir de voir tous ces gens aussi motivés et investis dans leurs sujets.

J'ai aussi pu échanger avec eux sur le pourquoi du comment de leur sujets de conférence.
Ces échanges ont d'ailleurs été pour moi aussi intéressants
que les conférences auxquelles j'ai assité.


Le dîner des speakers
~~~~~~~~~~~~~~~~~~~~~

Pour finir la journée, tous les speakers se sont retrouvés autour d'un buffet.
Un grand moment de détente pour tout le monde:

-   les speakers qui ont fait leur(s) présentation(s) aujourd'hui soufflent
-   ceux qui ne l'ont pas encore faite, écoutent avec attention les conseils
    et retours des premiers.
-   les orgas (qu'il ne faut surtout pas oublier) se reposent et soufflent
    après cette première journée éprouvante.

C'est surtout un moment d'échanges et de discussions qui n'est pas possible dans la journée avec le rythme de présentations et le monde qui afflue de partout.

Bilan
~~~~~

Bilan de cette première journée: très positif !

Le point notable est la place prépondérante que prend le développement web
côté client dans les présentations.
Les mentalités et les outils évoluent sur ce sujet.

J'ai passé une très bonne journée, très enrichissante.
Cette journée a été l'occasion de revoir beaucoup d'anciennes connaissances
et de discuter sur des sujets aussi variés qu'intéressants.

Je remercie d'ailleurs `Valtech`_ qui m'a permis de participer à cet evènement.

Je rentre donc chez moi, les bras chargés de goodies

- T-Shirts
- Stylos
- Sac à dos
- Boite à "meuh"
- ...

Mais ce n'est pas fini, il reste encore 2 jours et une présentation à faire!

.. _Devoxx France 2013: http://www.devoxx.com

.. _Le fantôme, le zombie et le testacular: http://www.devoxx.com/display/FR13/Le+fantome%2C+le+zombie+et+testacular%2C+panorama+des+outils+de+tests+pour+application+web+moderne.

.. _Jean-Laurent de Morlhon: https://twitter.com/morlhon
.. _Pierre Gayvallet: https://twitter.com/wayofspark

.. _PhantomJS: http://phantomjs.org/
.. _CasperJS: http://casperjs.org/
.. _Zombie.js: http://zombie.labnotes.org/
.. _Testacular: http://karma-runner.github.com/
.. _Sinon.js: http://sinonjs.org/



.. _Frontend Live Coding: http://www.devoxx.com/display/FR13/Frontend+Live+Coding+++Tour+d%27horizon+de+l%27outillage+et+des+technos+web+d%27aujourd%27hui

.. _Frédric Camblor: https://twitter.com/fcamblor

.. _Yeoman: http://yeoman.io/
.. _Grunt: http://gruntjs.com/
.. _Sass/Compass: http://compass-style.org/
.. _LessCss: http://lesscss.org/
.. _Bower: http://twitter.github.com/bower/
.. _Require.js: http://requirejs.org/
.. _Backbone.js: http://backbonejs.org/
.. _Handlebar.js: http://handlebarsjs.com/
.. _Rivets.js: http://rivetsjs.com/


.. _`Space Chatons: Bleeding Edge HTML5`: http://www.devoxx.com/display/FR13/Space+Chatons++Bleeding+Edge+HTML5Beeding

.. _Philippe Antoine: https://twitter.com/PhilippeAntoine

.. _Valtech: http://www.valtech.fr
