Crypter ses mots de passe Maven
###############################
:date: 2012-02-18 17:54
:author: noirbizarre
:category: Java
:tags: maven
:lang: fr

Lorsque l'on veut publier sur un dépôt maven, il faut fournir un
identifiant et mot de passe que l'on peut stocker et crypter dans sont
fichier ``~/.m2/settings.xml`` tel qu'indiqué dans la `documentation officielle`_.

Malheureusement les commandes ``mvn --encrypt-master-password`` et
``mvn --encrypt-password`` demande d'inquer le mot de passe sur la ligne
de commande ce qui est peu secure puisqu'il apparaitra dans l'historique
du shell.

Pour contourner ce problème, il est possible de demander au shell de
demander le mot de passe pour ne pas laisser de trace dans l'historique:

.. code-block:: bash

    read -p "Password? " pass; mvn --encrypt-master-password $pass
    read -p "Password? " pass; mvn --encrypt-password $pass


.. _documentation officielle: http://maven.apache.org/guides/mini/guide-encryption.html
