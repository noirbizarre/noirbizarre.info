Et un pelican de plus dans le troupeau !
########################################

:date: 2013-03-17 23:00
:author: noirbizarre
:category: Blog
:tags: blog, migration, pelican, python, restructuredtext, wordpress
:lang: fr

Depuis quelques temps, beaucoup de bloggers migrent vers du blog statique et en particulier vers Pelican_.

Pour rappel, Pelican_ c'est:

- un moteur de blog statique écrit en Python
- un projet actif avec une bonne communauté
- du reStructuredText ou du Markdown pour l'édition
- jinja 2 comme moteur de template


Cela fait plusieurs années que je tourne sur Wordpress et j'ai moi aussi décidé de franchir le cap.


La migration s'est passé sans aucun soucis pour moi.
Je ne vais pas redétailler l'installation et la migration car il a énormément de documentation sur le net
entre la documentation officielle de Pelican et les différents articles de blog que l'on trouve assez facilement
(et qui en ce moment fleurissent).


Maintenant que c'est fait, le retour arrière est impossible !!
Quel plaisir d'éditer ses articles en reStructuredText depuis son editeur favoris !
Ajoutez à ceci un git post-receive hook sur ma dedibox et je ne suis plus qu'à un push de la publication !


Je pense que ma semaine sera placée sous le signe du Pelican avec à l'ordre du jour:

- suppressions des articles devenus obsolètes ou inutiles (catégorie Flex)
- finalisation et publication ou suppression des vieux brouillons non publiés
- un peu de mise en forme des pages statiques
- quelques ajustement sur mon thème maison
- quelques contributions à publier


Le seul point négatif pour l'instant: `Disqus`_.
Je n'ai rien à redire sur le service en lui même et je n'ai aucun soucis à lui déléguer la gestion des commentaires.
Le seul soucis: j'ai lancé l'import de mes anciens commentaires il y a 2 jours et le service d'import à l'air HS :(



.. _Pelican: http://getpelican.com
.. _Disqus: http://disqus.com/
