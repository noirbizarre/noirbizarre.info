Maven: l'essentiel
##################
:date: 2010-12-10 17:41
:author: noirbizarre
:category: Java
:tags: flex, java, maven
:status: draft
:lang: fr

Aujourd'hui il est difficile de toucher à Java sans n'avoir jamais
entendu parlé ou utilisé Maven, plus précisement Maven 2.

Cet outil que beaucoup comparent à Ant est en fait bien plus. Il permet
entre autre de:

-  standardiser le développement en java (arborescence, build, nommage, ...)
-  centraliser les informations liées à la construction et au déploiement d'un projet
-  faire abstraction de l'environnement de développement de chaque développeur
-  gérer des dépendances transitives
-  gagner du temps (beaucoup de temps) sur les opérations répétitives et standards
-  s'intégrer facilement à des outils de mesure et qualité (Hudson, Sonar, ...)


Les commandes de base de Maven sont:

-  ``mvn clean install`` : nettoyer et construie un projet.
-  ``mvn test`` : lancer le process de test d'un projet.


Dans le cadre d'un projet multimodules, les options suivantes sont intéréssantes:

-  ``mvn xxx -pl module1,module3`` : execute l'opération xxx sur les modules 1 et 3.
-  ``mvn xxx -rf module2``: execute l'opération xxx en partant du module 2.

Pour un projet J2EE standard,

Pour un module de type WAR, j'utilise les commandes suivantes:

-  ``mvn jetty:run`` : lance jetty sans crée le war.
   Les ressources statiques sont directement prises depuis src/main/webapp.
-  ``mvn jetty:run-war`` : construit le war et le déploie dans jetty.
-  ``mvn jetty:deploy-war`` : déploie la dernière version du war installée dans le dépôt maven.


Pour les commandes jetty:run-war et jetty:deploy-war, le war utilisé est
extrait dans le répertoires target/work/webapp du module.

Pour pouvoir debuguer pendant une execution maven il faut modifier la
variable d'environnement ``MVN\_OPTS`` comme suit:

.. code-block:: bash

    MAVEN_OPTS =


J'utilise aussi Maven pour construire mes projets Flex avec flex-mojos.
Les principales commandes que j'utilise sont:

-  ``mvn flexmojos:flexbuilder`` : crée un descripteur de projet eclipse/Flex Builder pour le module
