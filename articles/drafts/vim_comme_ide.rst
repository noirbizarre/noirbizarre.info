ViM comme IDE
#############
:date: 2011-01-11 15:40
:author: noirbizarre
:category: Développement
:tags: ide, nerdtree, python, taglist, vim
:status: draft
:lang: fr

ViM est un éditeur de texte puissant aux possibilité quasiment infinies.
Il est extensible par des plugins, personnalisable a souhait par des
scripts et il s'utilise en console.

Il est possible de le transformer en véritable environnement de
développement sur mesure, voici ma version.

Amélioration de la mise en page
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: Vim Script

   set nocompatible " Utilise les défauts Vim (bien mieux !)
   set bs=2 " Autorise l'effacement de tout en mode insertion
   set nu " Affiche les numéro de lignes
   set bg=dark " Change le jeux de couleur pour une meilleur lisibilité sur fond foncé

   set nobackup

   " Pour highlighter la ligne courante (pour mieux se repérer) en bleu :
   set cursorline "Affiche la ligne courante
   set ruler "Affiche la position du curseur
   set showmatch "Pour highlighter la ligne courante
   highlight CursorLine ctermbg=blue "Colore la ligne en bleue


Gestion de l'indentation
~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: Vim Script

   set preserveindent " Indispensable pour ne pas tout casser avec ce qui va suivre
   set autoindent
   set smartindent
   set shiftwidth=4 " Largeur de l'autoindentation
   set tabstop=4 " Largeur du caractère tab
   set softtabstop=4 " Largeur de l'indentation de la touche tab
   set smarttab " Utilise shiftwidth à la place de tabstop en début de ligne (et backspace supprime d'un coup si ce sont des espaces)


Gestion des onglets
~~~~~~~~~~~~~~~~~~~

ViM fourni une gestion natives des onglets

.. code-block:: Vim Script

   nmap <C-H> :tabp<CR>
   nmap <C-L> :tabn<CR>


Coloration synatxique et autocompletion
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Depuis la version 7, ViM supporte la completion du code.

La coloration syntaxique est quant à elle gérée depuis bien longtemps.

Il suffit juste de les activer en ajoutant au ``.vimrc``:

.. code-block:: Vim Script

   " Coloration Syntaxique et autcompletion
   syntax on " Active la colorisation syntaxique

   if has("autocmd")
      filetype on
      filetype plugin on
      autocmd FileType python set omnifunc=pythoncomplete#Complete
      autocmd FileType javascript set omnifunc=javascriptcomplete#CompleteJS
      autocmd FileType html set omnifunc=htmlcomplete#CompleteTags
      autocmd FileType css set omnifunc=csscomplete#CompleteCSS

      if has("autocmd") && exists("omnifunc")
         autocmd Filetype *

         if &omnifunc == "" |
            setlocal omnifunc=syntaxcomplete#Complete \|
         endif

      endif

   endif


Il suffit ensuite de faire CTRL-X CTRL-O pour afficher la popup d'autocompletion.

Pour plus d'information sur l'omnicompletion, consulter les pages d'aide
associées `:help new-omni-completion`_ et `:help compl-omni`_.

NERD Tree
~~~~~~~~~

`NERD Tree`_ permet d'afficher un explorateur de fichier dans une barre latérale.

TODO:

-  installation du plugin
-  les actions possibles


Le plus simple est de mapper l'action ``NERDTreeToggle`` à une touche

.. code-block:: Vim Script

   map <F2> :NERDTreeToggle<CR>


TagList
~~~~~~~

`Taglist`_ permet d'afficher un explorateur de code dans une barre latérale

.. code-block:: Vim Script

   map <F3> :TlistToggle<CR>
   let Tlist_Inc_Winwidth = 0
   let Tlist_Use_Right\_Window = 1

Pydoc
~~~~~

http://vim-fr.org/index.php/Pydoc


TaskList
~~~~~~~~

`TaskList`_ affiche la liste des tags TODO et FIXME comme Eclipse.


SnipMate
~~~~~~~~

.. code-block:: bash

   ~$ cd /tmp

   /tmp$ git clone git://github.com/msanders/snipmate.vim.git && cd snipmate.vim
   /tmp/snipmate.vim$ git checkout-index -a --prefix=$HOME/.vim/ && cd .. && rm -fr snipmate.vim


MiniBufExplorer
~~~~~~~~~~~~~~~

http://fholgado.com/minibufexpl

d sur un buffer pour le supprimer

Extended session manager
~~~~~~~~~~~~~~~~~~~~~~~~

http://www.vim.org/scripts/script.php?script\_id=3150

:SaveSession my-first-session

:OpenSession my-first-session


Mon fichier ~/.vimrc complet
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Voici mon fichier .vimrc complet.

.. code-block:: Vim Script

   set nocompatible " Utilise les défauts Vim (bien mieux !)
   set bs=2 " Autorise l'effacement de tout en mode insertion
   set nu " Affiche les numéro de lignes
   set bg=dark " Change le jeux de couleur pour une meilleur lisibilité sur fond foncé
   set nobackup

   " Indentation
   set preserveindent " Indispensable pour ne pas tout casser avec ce qui va suivre
   set autoindent
   set smartindent
   set shiftwidth=4 " Largeur de l'autoindentation
   set tabstop=4 " Largeur du caractère tab
   set softtabstop=4 " Largeur de l'indentation de la touche tab
   set smarttab " Utilise shiftwidth à la place de tabstop en début de ligne (et backspace supprime d'un coup si ce sont des espaces)

   " Pour highlighter la ligne courante (pour mieux se repérer) en bleu :
   set cursorline "Affiche la ligne courante
   set ruler "Affiche la position du curseur
   set showmatch "Pour highlighter la ligne courante
   highlight CursorLine ctermbg=blue " Colore la ligne en bleue

   " Coloration Syntaxique et autcompletion
   syntax on " Active la colorisation syntaxique
   if has("autocmd")
      filetype on
      filetype plugin on
      autocmd FileType python set omnifunc=pythoncomplete#Complete
      autocmd FileType javascript set omnifunc=javascriptcomplete#CompleteJS
      autocmd FileType html set omnifunc=htmlcomplete#CompleteTags
      autocmd FileType css set omnifunc=csscomplete#CompleteCSS
      if has("autocmd") && exists("omnifunc")
         autocmd Filetype \*
         if &omnifunc == "" \|
            setlocal omnifunc=syntaxcomplete#Complete \|
         endif
      endif
   endif

   " Taglist
   map <F3> :TlistToggle<CR>
   let Tlist\_Inc\_Winwidth = 0
   let Tlist\_Use\_Right\_Window = 1

   " NERD Tree
   map <F2> :NERDTreeToggle<CR>

   " Gestion des onglets
   nmap <C-H> :tabp<CR>
   nmap <C-L> :tabn<CR>

Il est assez facile de trouver des exemples sur le net notament sur:

-  `StackOverflow`_, en particulier `cette question`_
-  la `page dédiée à .vimrc sur vim-fr.org`_
-  la `page .vimrc sur dotfiles.org`_


Pour plus d'information sur .vimrc, il suffit de consulter l'aide `:help vimrc`_

Beaucoup d'autres plugins existent, il suffit de consulter `la liste sur
le site officiel de ViM`_ pour trouver ceux qui transformeront ViM en
IDE sur mesure.

.. _`:help new-omni-completion`: http://vimdoc.sourceforge.net/cgi-bin/help?tag=new-omni-completion
.. _`:help compl-omni`: http://vimdoc.sourceforge.net/cgi-bin/help?tag=compl-omni
.. _NERD Tree: http://www.vim.org/scripts/script.php?script_id=1658
.. _Taglist: http://vim.sourceforge.net/scripts/script.php?script_id=273
.. _TaskList: http://www.vim.org/scripts/script.php?script_id=2607
.. _StackOverflow: http://stackoverflow.com/questions/tagged/vimrc
.. _cette question: http://stackoverflow.com/questions/164847/what-is-in-your-vimrc
.. _page dédiée à .vimrc sur vim-fr.org: http://vim-fr.org/index.php/Vimrc
.. _page .vimrc sur dotfiles.org: http://dotfiles.org/.vimrc
.. _`:help vimrc`: http://vimdoc.sourceforge.net/cgi-bin/help?tag=vimrc
.. _la liste sur le site officiel de ViM: http://www.vim.org/scripts/
