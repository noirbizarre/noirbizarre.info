Les outils de build pour Python
###############################
:date: 2013-03-19 18:39
:author: noirbizarre
:category: Python
:tags: python, build, fabric, paver, shover, bento, makefile, buildout, setuptools, distribute
:status: draft
:lang: fr

setuptools et scripts sh
~~~~~~~~~~~~~~~~~~~~~~~~

Setuptools et distribute correspondent au standard de packagin Python.
Il fournissent les outils nécéssaires au packaging et à la gestion des dépendances.

Le problème est qu'il n'y a pas qu'une seule implémentationde référence:

 - distutils
 - setuptools
 - distribute
 - distutils2

On trouve quelques extensions pour setuptools.

Pour ne pas avoir à taper toutes les commandes et leur paramètre à chaque fois, on utilisera des scripts sh à côté pour faire la glue.


Makefile
~~~~~~~~

On ne le présente plus, c'est la standard incontesté des outils de build.

Il permettra de scripter toutes les tâches nécéssaires à votre workflow.
Le problème est sa syntaxe oldschool et son manque de souplesse.

La documentation est très facile à trouver sur le net.

Makefile est indépendant du language:il ne fournit aucun outil spécifique à Python.

Aucun support au déploiement n'est fourni non plus.

https://github.com/mwilliamson/python-makefile/blob/master/makefile
http://blog.bottlepy.org/2012/07/16/virtualenv-and-makefiles.html
TODO


Fabric
~~~~~~

C'est l'outil de déploiement Python par excellence.

La syntaxe Python permet de réaliser quasiement tout ce qui vous passe par la tête.

Le projet est activement maintenu, très bien documenté et facilement extensible.

Sa forte communauté fait qu'il est facile de trouver des exemples et des extensions comme `fabtools`_ ou `cuisine`_.

http://fabfile.org

Paver
~~~~~

http://paver.github.com/
TODO


Shovel
~~~~~~

https://github.com/seomoz/shovel
TODO


Bento
~~~~~

http://cournape.github.com/Bento
TODO


Buildout
~~~~~~~~

C'est l'une des références du build pour Python.

Il dispose d'une très forte communauté et son modèle d'extension, "les recettes" ou "recipies" fait qu'il est facile de trouver l'extension qu'il vous faut.

http://www.buildout.org/
TODO


Tableau comparatif
~~~~~~~~~~~~~~~~~~

=======================  ==============  ==========  =========  ==========  =========  ============  ===========
Critères                   Setuptools      Fabric      Paver      Shovel      Bento      Buildout      Makefile
=======================  ==============  ==========  =========  ==========  =========  ============  ===========
Documentation              \-              +++                                           \- \-         \+
syntaxe                    Python + sh     Python      Python     Python      Python     ini
Clareté de la syntaxe                                                                                  \-
Dépendances                ++              \+          ?          ?           \+         \-            \-
Extensibilité              \+              ++          ?          ?           ?          +++
Ecosystème                 \+              ++          ?          ?           ?          +++
=======================  ==============  ==========  =========  ==========  =========  ============  ===========

Conclusion
~~~~~~~~~~

Moi, personnellement, j'utilise Fabric + Fabtools parce que:

- sa documentation est complète
- le projet est très actif
- couvre build et déploiement
- peu de dépendances
- grosse communauté et beaucoup d'extensions

Lorsque je distribute un projet, je vais me rabattre sur le standard: setuptools/distribute et scripts sh.


.. _fabtools: https://github.com/ronnix/fabtools
.. _cuisine: https://github.com/sebastien/cuisine
