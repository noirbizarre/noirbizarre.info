Single page webapps with Django and Ember.js
############################################
:date: 2012-10-29 00:46
:author: noirbizarre
:category: Python
:tags: python, django, ember.js, webapp
:status: draft
:lang: fr

Structure
=========

TODO

Strategy 1: a single page webapp by Django app
----------------------------------------------

TODO

Strategy 2: a single page webapp for the whole Django project
-------------------------------------------------------------

TODO

Data
====

TODO

Django.js + homemade ajax views
-------------------------------

TODO

Ember Data + Tastypie
---------------------

TODO
