Developpement web local avec virtualbox
#######################################
:date: 2012-05-01 15:16
:author: noirbizarre
:category: Développement
:tags: debian, NAT, private network, vhost, virtualbox, virtualhost
:lang: fr

Le développement web sur son poste local avec une configuration de test
est classique. Seulement, il est nécessaire de pouvoir tester sa
configuration en déploiement réel sur un serveur.

Pour cela, j'utilise Virtualbox pour créer des serveurs virtuels (Debian
en général) avec les contraintes suivantes:

-  le serveur doit pouvoir accéder à internet simplement
-  le serveur doit être accessible depuis mon poste de travail (ssh,
   http, ...)
-  la configuration de virtualhosts doit être simple
-  aucune connection à internet ne doit être nécessaire pour accéder au
   serveur depuis son IP public


Pour répondre à ces contraintes, je crée des machine virtuelles avec 2
interfaces:

-  eth0 sur le NAT virtualbox
-  eth1 sur le réseau privé virtuel


Il faut donc activer le réseau privé virtuel sous VirtualBox en cliquant
sur "+" dans *Fichiers > Paramètres... > Réseau*:

.. image:: |filename|/images/vbox/vbox-network-parameters.jpeg
   :alt: Paramètres réseau VirtualBox
   :align: center


Une fois l'interface crée, il faut la configurer. Dans notre cas, le
réseau est 192.168.77.0 et le serveur DHCP est désacttivé:

.. image:: |filename|/images/vbox/vbox-private-network-paramater.jpeg
   :alt: Paramètres du réseau privé virtuel
   :align: center

.. image:: |filename|/images/vbox/vbox-private-network-paramater-dhcp.jpeg
    :alt: Paramètres DHCP du réseau privé virtuel
    :align: center

Maintenant que ce réseau existe, il fautconfigurer notre VM pour qu'elle
utilise à la fois le NAT pour l'accès internet et le réseau privé
virtuel pour l'IP public locale:

.. image:: |filename|/images/vbox/vbox-vm-eth0.jpeg
    :alt: Paramètres de l'interface eth0
    :align: center


.. image:: |filename|/images/vbox/vbox-vm-eth1.jpeg
    :alt: Paramètres de l'interface eth1
    :align: center


Voici le fichier ``/etc/network/interfaces`` coorespondant sur la VM:

.. code-block:: bash

    # The loopback network interface
    auto lo
    iface lo inet loopback

    # The NATed network interface
    auto eth0
    allow-hotplug eth0
    iface eth0 inet dhcp

    # The public network interface
    auto eth1
    allow-hotplug eth1
    iface eth1 inet static
        address 192.168.77.2
        netmask 255.255.255.0
        network 192.168.77.0
        broadcast 192.168.77.255


Le point à noter: il ne faut pas déclarer de gateway pour l'interface
eth1. Tout le traffic sortant (accès à internet ou au réseau local) se
fait par l'interface eth0 configurée en DHCP sur le NAT Virtualbox.

Pour gérer mes virtualhosts, la configuration se fait à 2 niveaux:

-  dans le fichier ``/etc/hosts`` de ma machine hôte::

    192.168.77.2 vhost.dev

-  dans le fichier ``/etc/hosts`` de ma machine virtuelle::

    127.0.1.1 vhost.dev


Maintenant, cette machine virtuelle est accessible par vhost.dev, que ce
soit par SSH ou HTTP, et ce avec ou sans connection internet disponible.

Pour faciliter, le démarrage et l'arrêt de la VM, je rajoute 2 alias
dans mon fichier ``~/.bash_aliases``:

.. code-block:: bash

    alias start-vm='VBoxHeadless --startvm "my-vm"&'
    alias stop-vm='VBoxManage controlvm "my-vm" poweroff'
