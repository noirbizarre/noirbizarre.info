Git: l'essentiel
################

:date: 2010-12-06 17:23
:author: noirbizarre
:category: Développement
:tags: git
:lang: fr

Git est un scm qu'on ne présente plus. Bien que très pratique il peut se révéler très compliqué à utiliser.

Les commandes de base
~~~~~~~~~~~~~~~~~~~~~


Voici les commandes que j'utilise le plus:

-  ``git commit`` : publie localement les changements.
-  ``git checkout myBranch`` : change la branche courante pour myBranch.
-  ``git checkout -b myNewBranch`` : crée la branche myNewBranch et la définie comme branche active.
-  ``git branch`` : liste les branches locales.
-  ``git branch -a`` : liste toutes les branches.
-  ``git status`` : liste tous les changements non publiés.
-  ``git stash`` : met de côté tous les changements effectués depuis le dernier commit.
-  ``git stash apply`` : restaure les changement mis de côté avec git stash.
-  ``git log -n`` : liste les n derniers commits.


Le fichier ~/.gitconfig
~~~~~~~~~~~~~~~~~~~~~~~

git gère 3 niveaux de configuration:

#. Le niveau système dans ``/etc/gitconfig`` qui s'applique à tous les utilisateurs
#. Le niveau global dans ``~/.gitconfig`` qui s'applique à l'utilisateur courant
#. Le niveau dépôt dans ``$GIT_REPO/.git/config`` qui s'applique uniquement au repository courant

Celui qui nous intéresse en l’occurrence est le niveau global.

Pour commencer, il va contenir la section user permettant de nous identifier en éditant le fichier ``~/.gitconfig``:

.. code-block:: ini

   [user]
   name = Axel H.
   email = noirbizarre@gmail.com

Pour certains projet, il pourra être intéressant de modifier ces informations d'identification.

Ces informations pourront être spécifiées directement depuis le shell:

.. code-block:: bash

   ~$ git config --global user.name "Axel H."
   ~$ git config --global user.email noirbizarre@gmail.com

D'autres options pourront être ajoutées, telle que la coloration:

.. code-block:: ini

   [color]
   diff = auto
   status = auto
   branch = auto
   interactive = auto
   ui = true
   pager = true

ou encore des alias:

.. code-block:: ini

   [alias]
   co = checkout
   ci = commit
   st = status
   br = branch

Avec ces alias on pourra tapper:

.. code-block:: bash

   ~$ git co mabranch
   ~$ git ci -m "Mon commentaire"
   ~$ git st

Il est possible de trouver énormément d'options à rajouter dans ``~/.gitconfig`` sur le web,
notemment sur `StackOverflow`_, et particulièrement `ce thread`_.

Comme d'habitude, pas de secrets, tout est dans le `man`_.

Le fichier .gitignore
~~~~~~~~~~~~~~~~~~~~~

Le fichier .gitignore permet de spécifier une liste de fichiers qui ne seront jamais pris en compte par git.

Je ne referais pas la documentation officielle, mais ce qu'il faut retenir c'est:

-  Les lignes commençant par # sont des commentaires
-  le ! permet de prendre la négation d'un pattern
-  le / en début de pattern spécifie la racine par rapport au fichier .gitignore
-  le / en fin de pattern spécifie un répertoire et tous ses sous répertoires
-  tous les `shell glob patterns`_ sont pris en charge

Pour une documentation complète sur gitignore, la `page man`_ reste la référence.

Il est possible de spécifier un ``.gitignore`` global, par exemple:

.. code-block:: bash

   ~$ echo "*~" >> ~/.gitignore
   ~$ echo "*.swp" >> ~/.gitignore
   ~$ echo "*.pyc" >> ~/.gitignore
   ~$ git config --global core.excludesfile ~/.gitignore

Cela permet d'éviter de ressaisir les fichiers qui sont toujours à
exclure de git (fichiers temporaires, produits de compilation, ...).

.. _StackOverflow: http://stackoverflow.com/
.. _ce thread: http://stackoverflow.com/questions/267761/what-does-your-gitconfig-contain
.. _man: http://www.kernel.org/pub/software/scm/git/docs/git-config.html
.. _shell glob patterns: http://en.wikipedia.org/wiki/Glob_(programming)
.. _page man: http://www.kernel.org/pub/software/scm/git/docs/gitignore.html
