Extraire les icones d'une font
##############################
:date: 2013-09-20 12:00
:author: noirbizarre
:category: Développement
:tags: font, fontforge, svg, woff, ttf, glyph, icon
:status: draft
:lang: fr

Aujourd'hui j'ai eu besoin d'extraire toutes les icones d'une font
afin de pouvoir les réutiliser dans une autre font créée avec `grunt-webfont`_.

C'est là que `FontForge`_, vient à la rescousse.
`FontForge`_ est une boîte à outil libre pour la création et l'édition de fonts
qui prend en charge à peu prêt tous les formats existants.
Cet outil est scriptable et permet d'exporter chaque glyphe au format EPS ou SVG.

Nous allons donc créer 2 scripts:

- ``font2svg.pe`` pour exporter en SVG

.. code-block:: bash

    Open($1)
    SelectWorthOutputting()
    Export("svg")


- ``font2eps.pe`` pour exporter en EPS

.. code-block:: bash

    Open($1)
    SelectWorthOutputting()
    Export("eps")

Il suffit ensuite de les invoquer avec l'une des commandes suivantes:

.. code-block:: console

    $ fontforge -lang=ff -script font2eps.pe [fontfile]
    $ fontforge -lang=ff -script font2svg.pe [fontfile]

où ``fontfile`` peut-être n'importe quel fichier de font pris en charge par `FontForge`_.

Après execution de cette ligne commande, vous aurez dans le même répertoire que la font un fichie SVG ou EPS par icone de la fonte.


.. _grunt-webfont: https://github.com/sapegin/grunt-webfont
.. _FontForge: http://fontforge.org/
