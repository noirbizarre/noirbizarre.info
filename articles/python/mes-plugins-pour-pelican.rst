Mes plugins pour Pelican
########################
:date: 2013-06-20 13:09
:author: noirbizarre
:category: Python
:tags: pelican, python, social, plugins, microdata
:lang: fr

Depuis mon passage à `Pelican`_ comme moteur de blog,
j'ai créé deux plugins que je veux vous présenter.

Ces plugins ne sont pas dans le dépôt :github:`getpelican/pelican-plugins`
car je n'adhère pas forcement au principe de dépôt unique pour tous les plugins.

Je préfère simplement les publier en tant que paquets python standard.

Ces deux plugins sont bien sûr open-sources et testés et toute contribution est la bienvenue.

`Pelican Microdata`_
~~~~~~~~~~~~~~~~~~~~

Ce plugin permet d'insérer des marqueurs sémantiques `Microdata`_ (ou microdonnées en français).

Ces marqueurs permettent au moteur de recherche de mieux indexer le contenu de votre site
puisqu'ils structurent sémantiquement son contenu.

Je ne ferai pas ici un cours sur `Microdata`_ car il est assez facile d'`en trouver sur le net`_.

Ce plugin fourni deux directives:

-   ``itemscope``, une directive de type `block` permettant d'ajouter un attribut ``itemscope`` à un tag:

    .. code-block:: ReST

        .. itemscope:: <Schema type>
            :tag: element type (défaut: div)
            :itemprop: attribut itemprop optionnel (pour l'imbrication)
            :compact: optionnel

            Contenu du bloc

-   ``itemprop``, une directive de type `inline` pour annoter du texte avec un attribut ``itemprop``.

    .. code-block:: ReST

        :itemprop:`Texte affiché <itemprop name>`
        :itemprop:`Texte affiché <itemprop name:http://some.url/>`


Par exemple, ce document en reStructuredText (issue de `la page google sur les microdonnées`_):

.. code-block:: ReST

    .. itemscope:: Person
        :tag: p

        Je m'appelle :itemprop:`Pierre Dumoulin <name>`,
        mais on m'appelle :itemprop:`Pierrot <nickanme>`.
        Voici ma page d'accueil :
        :itemprop:`www.exemple.com <url:http://www.example.com>`
        J'habite à Strasbourg, en Alsace, et je suis :itemprop:`ingénieur <title>`
        chez la :itemprop:`société ABC <affiliation>`.


sera rendu comme suit:

.. code-block:: html

    <p itemscope itemtype="http://data-vocabulary.org/Person">
        Je m'appelle <span itemprop="name">Pierre Dumoulin</span>,
        mais on m'appelle <span itemprop="nickname">Pierrot</span>.
        Voici ma page d'accueil :
        <a href="http://www.example.com" itemprop="url">www.example.com</a>
        J'habite à Strasbourg, en Alsace, et je suis <span itemprop="title">ingénieur</span>
        chez la <span itemprop="affiliation">société ABC</span>.
    </p>


Le deuxième exemple, plus complexe (avec des blocs imbriqués), de cette même page s’écrirait:

.. code-block:: ReST

    .. itemscope:: Person
        :compact:

        Je m'appelle :itemprop:`Pierre Dumoulin <name>`,
        mais on m'appelle :itemprop:`Pierrot <nickanme>`.
        Voici ma page d'accueil :
        :itemprop:`www.exemple.com <url:http://www.example.com>`
        J'habite à

        .. itemscope:: Address
            :tag: span
            :itemprop: address

            :itemprop:`Strasbourg <locality>`, :itemprop:`Alsace <region>`,

        et je suis :itemprop:`ingénieur <title>`
        chez :itemprop:`Tartampion <affiliation>`.


Il aurait exactement le rendu suivant, où l'adresse est à la fois un ``itemprop`` de type ``address``
et un ``itemscope`` de type ``Address``:

.. code-block:: html

    <div>
        Je m'appelle <span itemprop="name">Pierre Dumoulin</span>,
        mais on m'appelle <span itemprop="nickname">Pierrot</span>.
        Voici ma page d'accueil :
        <a href="http://www.example.com" itemprop="url">www.example.com</a>.

        J'habite à
        <span itemprop="address" itemscope
          itemtype="http://data-vocabulary.org/Address">
          <span itemprop="locality">Strasbourg</span>,
          <span itemprop="region">Alsace</span>
        </span>
        et je suis <span itemprop="title">ingénieur</span>
        chez <span itemprop="affiliation">Tartampion</span>.
    </div>


Le mot clef ``compact`` permet d'éviter la création de blocs ``<p>`` sous le bloc ``itemscope``.

L'installation est tout à fait classique:

#.  installation avec pip:

    .. code-block:: bash

        ~$ pip install pelican-microdata


#.  ajout du plugin dans la configuration de Pelican:

    .. code-block:: python

        PLUGINS = (
            'microdata',
        )

`Pelican Social`_
~~~~~~~~~~~~~~~~~

Ce plugin permet d'insérer facilement des liens vers du contenu de différents réseaux sociaux.

L'idée m'est venue lorsque j’écrivais mes articles sur Devoxx
et que je me suis retrouvé à écrire toutes les URL vers les différents profils de conférenciers une par une.
C'est long, fastidieux et répétitif.
Les URL sont toujours les mêmes, seul le nom d'utilisateur importe.

Avec ce plugin, lier à du contenu de réseau social devient rapide et facile:

.. code-block:: ReST

    Lier à :twitter:`mon profile twitter <noirbizarre>`
    ou :github:`un dépôt github <noirbizarre/django.js>`
    devient facile.

Cette première version permet de créer simplement des liens vers:

- des profils `Twitter`_
- des profils `Facebook`_
- des profils `Google+`_
- des profils, dépôts et issue/pull-requests `Github`_

Les prochaines versions devraient ajouter de nouveaux réseaux sociaux,
le support de nouveaux types de contenu ainsi que le support de Python 3.

L'installation est elle aussi tout à fait classique:

#.  installation avec pip:

    .. code-block:: bash

        ~$ pip install pelican-social


#.  ajout du plugin dans la configuration de Pelican:

    .. code-block:: python

        PLUGINS = (
            'social',
        )

N'hésitez pas à suggérer d'autres réseaux sociaux ou à contribuer pour supporter de nouveaux types de contenus.

.. _Pelican: http://getpelican.com/
.. _Microdata: http://schema.org/
.. _Twitter: https://www.twitter.com/
.. _Facebook: https://www.facebook.com/
.. _Google+: https://plus.google.com/
.. _Github: https://github.com/

.. _Pelican Microdata: https://github.com/noirbizarre/pelican-microdata
.. _Pelican Social: https://github.com/noirbizarre/pelican-social
.. _mes articles sur Devoxx: http://noirbizarre.info/tag/devoxx/

.. _la page google sur les microdonnées: http://support.google.com/webmasters/bin/answer.py?hl=fr&answer=176035
.. _en trouver sur le net: https://www.google.fr/search?q=microdata
