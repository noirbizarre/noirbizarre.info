pip, virtualenv, virtualenvwrapper et pydev
###########################################

:date: 2010-07-10 20:51
:author: noirbizarre
:category: Python
:tags: eclipse, pip, pydev, python, virtualenv
:lang: fr

L'une des difficultés majeures du développement aujourd'hui est la
gestion des dépendances et en particulier pour Python l'isolement de
l'environnement de développement.

Pour résoudre ce problème, nous allons utiliser :

-  `pip`_: un remplaçant d'easy\_install bien plus complet.
-  `virtualenv`_: permet d'instancier des environnements python isolés.
-  `virtualenvwrapper`_: facilite l'utilisation de virtualenv


﻿Installation des outils de développement
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


Si ce n'est déjà fait, il faut installer un environnement de
développement python classique:

.. code-block:: bash

    ~$ sudo aptitude install python-setuptools python-dev build-essential

Nous installons la dernière version de pip:

.. code-block:: bash

    ~$ sudo easy_install pip

Puis nous installons virtualenv et virtualenvwrapper

.. code-block:: bash

    ~$ sudo pip install virtualenv virtualenvwrapper

Il faut maintenant configurer notre environnement en ajoutant au fichier ``~/.bahrc``:

.. code-block:: bash

    # Python pip / virtualenv / virtualenvwrapper
    export WORKON_HOME=$HOME/.virtualenvs
    export PIP_VIRTUALENV_BASE=$WORKON_HOME
    export PIP_RESPECT_VIRTUALENV=true
    source /usr/local/bin/virtualenvwrapper.sh


Puis nous initialisons l'environnement:

.. code-block:: bash

    ~$ mkdir ~/.virtualenvs
    ~$ source ~/.bashrc
    virtualenvwrapper.user_scripts Creating /home/noirbizarre/.virtualenvs/initialize
    virtualenvwrapper.user_scripts Creating /home/noirbizarre/.virtualenvs/premkvirtualenv
    virtualenvwrapper.user_scripts Creating /home/noirbizarre/.virtualenvs/postmkvirtualenv
    virtualenvwrapper.user_scripts Creating /home/noirbizarre/.virtualenvs/prermvirtualenv
    virtualenvwrapper.user_scripts Creating /home/noirbizarre/.virtualenvs/postrmvirtualenv
    virtualenvwrapper.user_scripts Creating /home/noirbizarre/.virtualenvs/predeactivate
    virtualenvwrapper.user_scripts Creating /home/noirbizarre/.virtualenvs/postdeactivate
    virtualenvwrapper.user_scripts Creating /home/noirbizarre/.virtualenvs/preactivate
    virtualenvwrapper.user_scripts Creating /home/noirbizarre/.virtualenvs/postactivate
    virtualenvwrapper.user_scripts Creating /home/noirbizarre/.virtualenvs/get_env_details

Nous sommes maintenant prêts à instancier notre premier environnement.

Création d'un environnement isolé
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Il suffit maintenant d'utiliser la commande ``mkvirtualenv``.

.. code-block:: bash

    ~$ mkvirtualenv --no-site-packages myenv
    New python executable in myenv/bin/python
    Installing setuptools............done.
    virtualenvwrapper.user_scripts Creating /home/noirbizarre/.virtualenvs/myenv/bin/predeactivate
    virtualenvwrapper.user_scripts Creating /home/noirbizarre/.virtualenvs/myenv/bin/postdeactivate
    virtualenvwrapper.user_scripts Creating /home/noirbizarre/.virtualenvs/myenv/bin/preactivate
    virtualenvwrapper.user_scripts Creating /home/noirbizarre/.virtualenvs/myenv/bin/postactivate
    virtualenvwrapper.user_scripts Creating /home/noirbizarre/.virtualenvs/myenv/bin/get\_env\_details
    (myenv)~$


L'option ``--no-site-packages`` permet d'avoir une installation de
python nue, c'est à dire sans aucune des dépendances python ajoutées
dans l'installation courante.

Utilisation en ligne de commande
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Une fois l'environnement actif, le prompt du shell est préfixé par "(nom de l'environnement)".

Pour désactiver l'environnement, il suffit d'executer:

.. code-block:: bash

    (myenv)~$ deactivate
    ~$

Pour réactiver l'environnement il suffit d'executer:

.. code-block:: bash

    ~$ workon myenv
    (myenv)~$

La liste complète des commandes de virtualenvwrapper est disponible `ici`_.

Utilisation dans Eclipse/Pydev
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Pour utiliser cet environnement dans eclipse avec Pydev il suffit
d'aller dans *Preferences > Pydev > Interpreter Python* et d'ajouter
l'executable python qui correspond, dans notre cas ``~/.virtualenvs/myenv/bin/python``.

.. image:: |filename|/images/pydev/virtualenv_pydev_eclipse.png
   :alt: Configuration de l'executable python
   :align: center

Sur l'écran qui apparait ensuite, il ne faut selectionner que les chemin
qui correspondent à notre environnement (ceux commençant par ``~/.virtualenvs/myenv/``).

.. image:: |filename|/images/pydev/Selection-Needed.png
   :alt: Sélection des chemins
   :align: center

L'environnement apparait ensuite dans la liste des environnement disponibles.

.. image:: |filename|/images/pydev/Preferences_env_pydev.png
   :alt: Préférences Pydev de l'interpreteur python
   :align: center

Il suffit ensuite de le sélectionner dans les préférences des projets concernés.

.. image:: |filename|/images/pydev/myproject_pydev_interpreter.png
   :alt: Sélection de l'interpréteur python d'un projet
   :align: center

Utilisation de pip
~~~~~~~~~~~~~~~~~~

Lorsque l'environnement est actif, pip s'utilise comme apt/aptitude.

.. code-block:: bash

    # Recherche de paquets
    (myenv)~$ pip search mypackage

    # Installation d'un paquet
    (myenv)~$ pip install mypackage


Lorsque l'environnement est inactif, il est possible de specifier à pip
un environnement pour l'installation avec l'option ``-E``:

.. code-block:: bash

    ~$ pip install -E myenv mypackage

Il est possible de dumper tous les modules installés dans un fichier:

.. code-block:: bash

    ~$ pip freeze > requirements

Puis de les recharger d'un coup dans un autre environnement:

.. code-block:: bash

    ~$ pip install -r requirements

Pour plus de détails sur l'utilisation de pip, la `page officielle`_ fait office de documentation.

.. _pip: http://pip.openplans.org/
.. _virtualenv: http://virtualenv.openplans.org/
.. _virtualenvwrapper: http://www.doughellmann.com/projects/virtualenvwrapper/
.. _ici: http://www.doughellmann.com/docs/virtualenvwrapper/command_ref.html
.. _page officielle: http://pip.openplans.org/
