pip, virtualenv et virtualwrapper dans Cygwin
#############################################

:date: 2010-12-10 18:24
:author: noirbizarre
:category: Python
:tags: cygwin, pip, python, virtualenv
:lang: fr

Voici la marche à suivre pour installer ``Python``, ``pip``, ``virtualenv`` et
``virtualenvwrapper`` dans Cygwin (version 1.7.7-1 à ce jour).

Python doit être installé via l'installeur de Cygwin.

Pour installer pip, nous allons utiliser ``easy_install`` qui est fourni
par ``setuptools`` (version 0.6c11 au moment où j'écris ces lignes). Pour
trouver la dernière version en date correspondant à notre version de
Python (version 2.6.5 fournie par Cygwin), il faut se rendre sur `la
page officielle`_.

.. code-block:: bash

    ~$ wget http://pypi.python.org/packages/2.6/s/setuptools/setuptools-0.6c11-py2.6.egg
    ~$ sh setuptools-0.6c11-py2.6.egg
    ~$ easy_install pip


Une fois pip installé, il suffit de l'utiliser pour installer virtualenv et virtualenvwrapper

.. code-block:: bash

    ~$ pip install virtualenv virtualenvwrapper

On configure notre environnement dans ``~/.bashrc``:

.. code-block:: bash

    # Python pip / virtualenv / virtualenvwrapper
    export WORKON_HOME=$HOME/.virtualenvs
    export PIP_VIRTUALENV_BASE=$WORKON_HOME
    export PIP_RESPECT_VIRTUALENV=true
    source /usr/bin/virtualenvwrapper.sh


Ensuite, tout fonctionne comme dans `cet article`_.

.. _la page officielle: http://pypi.python.org/pypi/setuptools#files
.. _cet article: |filename|pip-virtualenv-et-pydev.rst
