Synchronisez vos notes Tomboy avec WebNotes
###########################################
:date: 2013-06-21 03:27
:author: noirbizarre
:category: Python
:tags: python, django, tomboy, webnotes
:status: draft
:lang: fr

Comme vous avez pu le remrquer, cette année c'est l'année des fermetures!
Entre la prtie de notes de Ubuntu One et Google Reader aujourd'hui,
ça fait déjà deux services que j'utilisais quotidiennement qui ferment!

Du coup, comme j'vais déjà une dedibox qui me servait à plusieurs choses (backup, test, dépôts git), j'ai décidé d'héberger un maximum de services chez moi et de redevelopper ceux qui me manque.

Voici le premier: `WebNotes`_, un serveur de synchonisation pour Tomboy.

Cette petite application Django permet de synchroniser vos notes comme vous l'auriez fait avant avec Ubuntu One.

Quelques petits screenshots:

.. image:: |filename|/images/webnotes/desktop.png
    :alt: La vue principale aka.le bureau
    :width: 100%
    :align: center

.. image:: |filename|/images/webnotes/note.png
    :alt: L'affichage d'une note
    :width: 100%
    :align: center

.. image:: |filename|/images/webnotes/note-out.jpg
    :alt: L'affichage d'une note détachée
    :width: 400px
    :align: center

Je l'utilise depuis maintenant 1 mois sans problème et j'ai décidé de la libérer.

La stack fera l'object d'une série d'articles sur ce blog, mais pour résumer:

- `Django REST Framework`_ pour gérer les APIs
- :gh:`Django.js <noirbizarre/django.js>` pour faire la glue entre JavaScript et Django
- :gh:`etianen/django-require` et :gh:`cyberdelia/django-pipeline` pour compiler Less, CSS et JavaScript
- :gh:`etianen/django-reversion` pour gérer les diffs
- `Bower`_ et `RequireJS`_ pour gérer les dépendances JavaScript
- `Backbone.js`_ et `Backbone.Marionette`_ côté client
- `Etch.js`_ comme éditeur
- Test javascript avec `Jasmine`_.

Tous ceux qui peuvent et veulent tester cette application sont invités àla faire pour faire remonter un maximum de bugs pour qu'il soit corrigés.
Toute contribution est bien sur la bienvenue.

Pour ceux qui n'en ont pas lapossibilité, j'envisage de fournir une version hébergée,
mais j'aimerai me pencher un peu plus sur les aspects sécurité avant.
Il faut que j'évalue le coût aussi pour savoir si je ne vais pas me ruiner pour faire tourner gracieusement ce service.

.. _webnotes: https://webnotes.io
.. _Django REST Framework: http://django-rest-framework.org/
.. _Bower: http://bower.io
.. _RequireJS: http://requirejs.org
.. _Backbone.js: http://backbonejs.org
.. _Backbone.Marionette: http://marionettejs.com
.. _Etch.js: http://etchjs.com
.. _Jasmine: http://pivotal.github.io/jasmine/
