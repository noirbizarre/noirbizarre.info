Création de virtualenvs préconfigurés hors-ligne
################################################
:date: 2013-03-05 21:27
:author: noirbizarre
:category: Python
:tags: pip, python, virtualenv, virtualenvwrapper
:lang: fr

Je crée régulièrement des virtualenvs avec virtualenvwrapper, et j'ai
toujours les mêmes problèmes:

-  il faut installer les mêmes outils de base à chaque fois
-  il faut être connecté pour les télécharger
-  le téléchargement peut être long

mkvirtualenv propose un hook, `postmkvirtualenv`_, qui permet d’exécuter
des commandes après la création du virtualenv.

Je l'utilise pour lancer l'installation de dépendances communes à tous
mes virtualenvs. Couplé avec la recette `Fast & Local Installs`_ de pip,
cela me permet de créer des virtualenvs préconfigurés totalement
hors-ligne.

Pour cela, on va créer le répertoire ``$WORKON_HOME/offline`` et le fichier
de requirement ``$WORKON_HOME/mkvirtualenv.pip``:

.. code-block:: bash

    ~$ mkdir $WORKON_HOME/offline
    ~$ cat $WORKON_HOME/mkvirtualenv.pip
    pep8
    sphinx
    ipdb
    ipython
    grin

Pour pouvoir instancier les virtualenvs sans connection, on va
télécharger les paquets dans ``$WORKON_HOME/offline``:

.. code-block:: bash

    pip install --download $WORKON_HOME/offline -r $WORKON_HOME/mkvirtualenv.pip

Il suffit maintenant d'éditer le fichier ``$WORKON_HOME/postmkvirtualenv``
pour effectuer l'installer des paquets à chaque création de virtualenv:

.. code-block:: bash

    #!/bin/bash
    # This hook is run after a new virtualenv is activated.
    pip install --no-index --find-links=file://$WORKON_HOME/offline -r $WORKON_HOME/mkvirtualenv.pip

Maintenant à la création de chaque virtualenv, mes outils sont installés
rapidement, plus de connection nécéssaire, plus de temps de
téléchargement...

Il est possible d'utiliser cette technique pour créer des templates très
simples. Par exemple, pour installer rapidement un environnement Django
dans un virtualenv:

.. code-block:: bash

    ~$ cat $WORKON_HOME/mkdjango.pip
    django
    django-jenkins
    django.js
    django-braces
    django-pipeline
    south
    pillow
    pytz
    ~$ cat $WORKON_HOME/mkdjango
    #!/bin/bash
    pip install --no-index --find-links=file://$WORKON_HOME/offline -r $WORKON_HOME/mkdjango.pip


Téléchargement des dépendances Django:

.. code-block:: bash

    pip install --download $WORKON_HOME/offline -r $WORKON_HOME/mkdjango.pip


Installation de Django dans un virtualenv existant et actif:

.. code-block:: bash

    (myvenv)~/workspace$ $WORKON_HOME/mkdjango


De la même façon, pour Fabric et Fabtools:

.. code-block:: bash

    ~$ cat $WORKON_HOME/mkfabric.pip
    fabric
    fabtools
    ~$ cat $WORKON_HOME/mkfabric
    #!/bin/bash
    pip install --no-index --find-links=file://$WORKON_HOME/offline -r $WORKON_HOME/mkfabric.pip


J'espère que cette technique vous sera aussi utile qu'à moi!

.. _postmkvirtualenv: http://virtualenvwrapper.readthedocs.org/en/latest/scripts.html#postmkvirtualenv
.. _Fast & Local Installs: http://www.pip-installer.org/en/latest/cookbook.html#fast-local-installs
