Jetty 7 et Hudson sous Debian Lenny
###################################

:date: 2011-01-07 03:35
:author: noirbizarre
:category: Admin
:tags: cherokee, debian, hudson, jetty, openjdk
:lang: fr

Dans cet article décrit l'installation en tant que service de Jetty 7
sur Debian Lenny ainsi que le déploiement de Hudson.

Installation d'OpenJDK 6 Headless
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Avant toute chose, il faut une installation de JRE fonctionnelle, dans
mon cas OpenJDK 6 Headless.

.. code-block:: bash

    noirbizarre:~$ sudo aptitude install openjdk-6-jre-headless


Installation de jetty
~~~~~~~~~~~~~~~~~~~~~

Nous allons créer un utilisateur jetty et le groupe associé et les
utiliser pour installer et exécuter jetty.

.. code-block:: bash

    noirbizarre:~$ sudo groupadd jetty
    noirbizarre:~$ sudo useradd -s /bin/bash -d /home/jetty -m -g jetty jetty
    noirbizarre:~$ sudo passwd jetty

Après voir récupéré la dernière version stable de Jetty 7 disponible
`ici`_, dans mon cas la version *7.2.2.v20101205.*, et on l'installe
dans le ``$HOME`` de notre utilisateur.

.. code-block:: bash

    noirbizarre:~$ sudo -i -u jetty
    jetty:~$ JETTY_VERSION=7.2.2.v20101205
    jetty:~$ wget http://download.eclipse.org/jetty/$JETTY_VERSION/dist/jetty-distribution-$JETTY_VERSION.tar.gz
    jetty:~$ tar -xvzf jetty-distribution-$JETTY_VERSION.tar.gz
    jetty:~$ ln -s jetty-distribution-$JETTY_VERSION jetty7


Nous allons lancer jetty une première fois pour vérifier que tout s'execute bien:

.. code-block:: bash

    jetty:~$ cd jetty7
    jetty:~/jetty7$ java -jar start.jar
    2011-01-07 00:58:19.337:INFO::jetty-7.2.2.v20101205
    2011-01-07 00:58:19.410:INFO::Deployment monitor /home/jetty/jetty-distribution-7.2.2.v20101205/webapps at interval 1
    2011-01-07 00:58:19.426:INFO::Deployment monitor /home/jetty/jetty-distribution-7.2.2.v20101205/contexts at interval 1
    2011-01-07 00:58:19.432:INFO::Deployable added: /home/jetty/jetty-distribution-7.2.2.v20101205/contexts/test.xml
    2011-01-07 00:58:19.568:INFO::Extract jar:file:/home/jetty/jetty-distribution-7.2.2.v20101205/webapps/test.war!/ to /tmp/jetty-0.0.0.0-8080-test.war-\_-any-/webapp
    2011-01-07 00:58:21.438:INFO:org.eclipse.jetty.servlets.TransparentProxy:TransparentProxy @ /javadoc to http://download.eclipse.org/jetty/stable-7/apidocs
    2011-01-07 00:58:21.441:INFO::Deployable added: /home/jetty/jetty-distribution-7.2.2.v20101205/contexts/javadoc.xml
    2011-01-07 00:58:21.523:INFO::Started SelectChannelConnector@0.0.0.0:8080


Une fois l'initialisation terminée, on peut se connecter sur http://localhost:8080
pour afficher la page d'accueil de Jetty ainsi que quelques exemples.

On peut maintenant couper le serveur et quitter la session de
l'utilisateur jetty.

Déploiement de jetty en tant que service
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Pour démarrer Jetty en tant que service on utilise le script fourni dans le repertoire ``bin``.

.. code-block:: bash

    noirbizarre:~$ sudo cp /home/jetty/jetty7/bin/jetty.sh /etc/init.d/jetty
    noirbizarre:~$ sudo chmod u+x /etc/init.d/jetty


Toutes les variables d'environnement sont à déclarer dans ``/etc/default/jetty``:

.. code-block:: bash

    JETTY_USER=jetty
    JETTY_HOME=/home/$JETTY_USER/jetty7


.. code-block:: bash

    noirbizarre:~$ sudo /etc/init.d/jetty check
    Checking arguments to Jetty:
    JETTY_HOME = /home/jetty/jetty7
    JETTY_CONF = /home/jetty/jetty7/etc/jetty.conf
    JETTY_RUN = /var/run
    JETTY_PID = /var/run/jetty.pid
    JETTY_PORT =
    JETTY_LOGS =
    START_INI = /home/jetty/jetty7/start.ini
    CONFIGS = --pre=etc/jetty-logging.xml
    JAVA_OPTIONS = -Djetty.home=/home/jetty/jetty7 -Djava.io.tmpdir=/tmp
    JAVA = /usr/bin/java
    CLASSPATH =
    RUN_CMD = /usr/bin/java -Djetty.home=/home/jetty/jetty7 -Djava.io.tmpdir=/tmp -jar /home/jetty/jetty7/start.jar --pre=etc/jetty-logging.xml

    noirbizarre:~$ sudo /etc/init.d/jetty
    Usage: jetty [-d] {start|stop|run|restart|check|supervise} [ CONFIGS ... ]
    noirbizarre:~$ sudo /etc/init.d/jetty start
    Starting Jetty: OK
    noirbizarre:~$ sudo /etc/init.d/jetty stop
    Stopping Jetty: OK


Une fois toutes les vérifications effectuées, pour lancer Jetty au démarrage du serveur:

.. code-block:: bash

    noirbizarre:~$ sudo update-rc.d jetty defaults


Installation de Hudson
~~~~~~~~~~~~~~~~~~~~~~

On installe Hudson avec l'utilisateur jetty en lui specifiant de travailler dans le répertoire ``/home/jetty/hudson``:

.. code-block:: bash

    noirbizarre:~$ echo 'export HUDSON_HOME=/home/$JETTY_USER/hudson' | sudo tee -a /etc/default/jetty
    noirbizarre:~$ sudo -i -u jetty
    jetty:~$ cd jetty7/webapps
    jetty:~/jetty7/webapps$ wget http://mirrors.hudson-labs.org/war/latest/hudson.war


Il n'y a plus qu'a démarrer jetty et vérifier que http://localhost:8080/hudson affiche bien l'accueil de Hudson.

.. _ici: http://download.eclipse.org/jetty/stable-7/dist/
