Dokuwiki et Cherokee
####################

:date: 2010-12-04 16:09
:author: noirbizarre
:category: Admin
:tags: cherokee, dokuwiki, url rewrite
:lang: fr

Pour faire tourner une installation secure de dokuwiki dans Cherokee
avec les spécifications suivantes:

-  Accessible depuis https://wiki.noirbizarre.info/

   -  uniquement https
   -  à la racine du domaine
   -  certificat autosigné

-  Récriture des URLs propres
-  DocumentRoot situé dans /var/www/noirbizarre.info/wiki
-  Logs situés dans /var/logs/cherokee/noirbizarre.info (wiki.access et wiki.errors)


Ce tutoriel a été rédigé pour une version 1.0.8 de Cherokee tournant
sous Debian Lenny. Les libellés et les screenshots peuvent varier d'une
version à l'autre.

Installation du wiki
--------------------


Pour commencer on récupère la dernière version de dokuwiki `ici`_ et on crée l'arborescence nécéssaire.

.. code-block:: bash

   ~$ wget http://www.splitbrain.org/_media/projects/dokuwiki/dokuwiki-2010-11-07.tgz
   ~$ sudo mkdir -p /var/www/noirbizarre.info
   ~$ tar xvzf ﻿dokuwiki-2010-11-07.tgz
   ~$ sudo mv dokuwiki-2010-11-07 /var/www/noirbizarre.info/wiki
   ~$ sudo chown -R www-data:www-data /var/www/noirbizarre.info/wiki
   ~$ sudo mkdir -p /var/log/cherokee/noirbizarre.info
   ~$ sudo chown -R www-data:www-data /var/log/cherokee/noirbizarre.info


Génération du certificat
~~~~~~~~~~~~~~~~~~~~~~~~


Pour cet exemple, nous allons utilisé un certificat autosigné. Il permet
de crypter la communication mais le fait qu'il soit auto signé ne permet
d'authentifier l'hôte distant. OpenSSL doit être installé pour cette
étape. En répondant aux questions, on s'assurera que ``Common Name`` est
bien rempli avec le nom de domaine (Cherokee s'en sert pour le routage).

.. code-block:: bash

   ~$ openssl req -new -x509 -nodes -out wiki.crt -keyout wiki.key -days 3650
   Generating a 1024 bit RSA private key

   ...............................................++++++
   .++++++
   writing new private key to 'wiki.key'
   -----
   You are about to be asked to enter information that will be incorporated into your certificate request.
   What you are about to enter is what is called a Distinguished Name or a DN.
   There are quite a few fields but you can leave some blank
   For some fields there will be a default value,
   If you enter '.', the field will be left blank.
   -----
   Country Name (2 letter code) [AU]:FR
   State or Province Name (full name) [Some-State]:France
   Locality Name (eg, city) []:
   Organization Name (eg, company) [Internet Widgits Pty Ltd]:noirbizarre.info
   Organizational Unit Name (eg, section) []:WiKi
   Common Name (eg, YOUR name) []:wiki.noirbizarre.info
   Email Address []:
   ~$ mv wiki.crt wiki.key /var/www/noirbizarre.info


Création du vServer
~~~~~~~~~~~~~~~~~~~

Pour cette étape, nous considérons que Cherokee est installé,
fonctionnel, configuré pour géré le SSL sur le port 443 et tourne avec
les droits de ``www-data:www-data``.

On se connecte sur l'interface d'administration via http://localhost:9090.

On crée un nouveau vServer nommé *wiki.noirbizarre.info* et ayant pour DocumentRoot ``/var/www/noirbizarre.info/wiki``.

.. image:: |filename|/images/dokuwiki/wiki_new_vserver.png
   :alt: Ajout d'un vServer pour le wiki
   :align: center


Le nom du vServer est important, il permet à Cherokee de router les
requetes vers le bon vServer.

Gestion des logs
^^^^^^^^^^^^^^^^

Dans l'onglet journalisation du vServer créé, spécifier:

-  Dans *Error Logging*:

   -  *Write errors to*: *fichier*
   -  *Nom de fichier*:
      ``/var/log/cherokee/noirbizarre.info/wiki.errors``

-  Dans *Access Logging*:

   -  *Nom de fichier*:
      ``/var/log/cherokee/noirbizarre.info/wiki.access``


.. image:: |filename|/images/dokuwiki/wiki_journalisation.png
   :alt: Journalisation
   :align: center


Gestion du SSL
^^^^^^^^^^^^^^

Sur l'onglet sécurité on spécifie les chemins des fichiers .crt et .key générés.

.. image:: |filename|/images/dokuwiki/wiki_certificats.png
   :alt: Gestion des certificats
   :align: center


Règles de gestion
^^^^^^^^^^^^^^^^^

Sur l'onglet *Behavior*, on clique sur *Rule Management* pour ouvrir le gestionnaire de règle
et on supprimer les 2 règles *Contenu statique* créées par défaut de façon à ne garder que la
règle *Par défaut*.

Nous allons maintenant modifier la règle *Par défaut* pour lui faire
prendre en charge les redirections et ajouter 3 règles supplémentaires:

-  Gestion du contenu statique
-  Gestion du php
-  Redirection vers https


Règle Par défaut
''''''''''''''''

Sur l'onglet *Gestionnaire*, on choisit *Redirection* dans la
liste déroulante et on rentre les règles internes suivantes:

-  ``^/\_media/([^/]+)\\?(.+)$`` → ``/lib/exe/fetch.php?media=$1&$2``
-  ``^/\_media/([^/]+)$`` → ``/lib/exe/fetch.php?media=$1``
-  ``^/\_detail/([^/]+)\\?(.+)$`` → ``/lib/exe/detail.php?media=$1&$2``
-  ``^/\_detail/([^/]+)$`` → ``/lib/exe/detail.php?media=$1``
-  ``^/\_export/([^/]+)/(.\*)$`` → ``/doku.php?do=export\_$1&id=$2``
-  ``^/$`` → ``/doku.php``
-  ``^/\\?([^/]+)$`` → ``/doku.php?$1``
-  ``^/([^/]+)\\?(.\*)$`` → ``/doku.php?id=$1&$2``
-  ``^/([^/]+)$`` → ``/doku.php?id=$1``


.. image:: |filename|/images/dokuwiki/wiki_regex.png
   :alt: Règles de redirection
   :align: center


L'ordre est important puisque Cherokee s’arrêtera à la première règle
qui match.

On pourra aussi autoriser la compression ***gzip*** et ***deflate***
depuis l'onglet **Encodage** pour optimiser les transferts, et ce pour
chaque règle que l'on rajoute.

.. image:: |filename|/images/dokuwiki/wiki_compression.png
   :alt: Encodage
   :align: center

Contenu statique
''''''''''''''''

On va spécifier à Cherokee que le fichier favicon.ico et le repertoire
``/lib`` sont du contenu statique et qu'il doit le traiter comme tel.

On rajoute alors la règle et le handler correspondant.

.. image:: |filename|/images/dokuwiki/wiki_static_rule.png
   :alt: Règle de contenu statique
   :align: center

.. image:: |filename|/images/dokuwiki/wiki_static_handler.png
   :alt: Gestionnaire de contenu statique
   :align: center

Il n'est pas nécéssaire de respécifier le DocumentRoot puisque celui du
vServer est repris par défaut.

Prise en charge du php
''''''''''''''''''''''

Pour cette étape, on considère que ``php5`` et ``php5-cgi`` sont installés.

On rajoute maintenant une règle extension pour capter tous les appels de
fichiers php.

Pour cela, Cherokee fourni un assitant qui créée automatiquement la
règle. On pourra personnaliser les valeurs, comme par exemple
l'encodage, ou encore la prise en charge de fcgi.

.. image:: |filename|/images/dokuwiki/wiki_php_support.png
   :alt: Assistant php
   :align: center

Il faut ensuite passer la règle en status *FINAL* en cliquant sur *NON FINAL* dans la liste des règles à gauche.

.. image:: |filename|/images/dokuwiki/wiki_rules_list.png
   :alt: Liste des règles et leur statut
   :align: center


Redirection vers HTTPS
''''''''''''''''''''''

Cette dernière règle va automatiquement rediriger l'utilisateur sur la
version sécurisée du site.

Pour cela on rajoute une règle *SSL/TLS* dont on va prendre la négation
et on y affecte un handler de *redirection externe* (visible par l'utilisateur).

.. image:: |filename|/images/dokuwiki/wiki_not_ssl.png
   :alt: Règle "n'est pas SSL"
   :align: center

.. image:: |filename|/images/dokuwiki/wiki_not_ssl_redirect.png
   :alt: Redirection vers la version sécurisée
   :align: center


Premier accès
~~~~~~~~~~~~~

Nous pouvons maintenant enregistrer nos changements et rédémarrer Cherokee.

.. image:: |filename|/images/dokuwiki/cherokee_restart.png
   :alt: Redémarrage de Cherokee
   :align: center

Nous pouvons maintenant accéder à https://wiki.noirbizarre.info/install.php et suivre la procédure d'installation.
Il faut pour cela que votre serveur DNS redirige bien *wiki.noirbizarre.info* vers votre serveur.

Un message d'avertissement nous signalera que notre certificat n'est pas sur puisqu'il est autosigné.

.. image:: |filename|/images/dokuwiki/dokuwiki_installer.png
   :alt: Installation de dokuwiki
   :align: center

Une fois l'installation effectuée, authentifiez-vous et dans les paramètres de configurations
spécifier les *URLs esthétiques* sur *.htaccess*.

.. image:: |filename|/images/dokuwiki/wiki_urlparam.png
   :alt: URLs esthétiques
   :align: center

Et voilà, vous pouvez maintenant tester la redirection HTTPS et les URLs esthétiques.
Tout fonctionne correctement.

.. _ici: http://www.splitbrain.org/projects/dokuwiki
