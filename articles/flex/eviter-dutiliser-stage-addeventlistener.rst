Eviter d'utiliser stage.addEventListener()
##########################################
:date: 2011-08-03 13:54
:author: noirbizarre
:category: Flex
:tags: eventlistener, flex, stage, systemmanager
:lang: fr

En Flex/ActionScript, il n'est pas rare de vouloir écouter un évènement
au niveau le plus haut et de voir du code tel que:

.. code-block:: as3

    if (stage) {
        stage.addEventListener(MonEvent.MON_TYPE, _monHandler);
    } else {
        this.addEventListener(Event.ADDED_TO_STAGE, function(e:Event):void{
            stage.addEventListener(MonEvent.MON_TYPE, _monHandler);
        });
    }


En effet, lors de l'inialisation des composants Flex, l'attribut ``stage`` n'est pas encore disponible.

Pourtant, il est plus simple et plus propre d'utiliser `SystemManager`_:

.. code-block:: as3

    systemManager.addEventListener(MonEvent.MON_TYPE, _monHandler);


Ce morceau de code ne plantera pas car ``systemManager`` est initilisé
dès le lancement de l'application et est toujours présent dans les
composants, contrairement à ``stage``.

.. _SystemManager: http://help.adobe.com/en_US/FlashPlatform/reference/actionscript/3/mx/managers/SystemManager.html
