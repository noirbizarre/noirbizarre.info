---
name: pelican-social
url: https://github.com/noirbizarre/pelican-social

started: 2013-06-20

badges:
   -  image: https://secure.travis-ci.org/noirbizarre/pelican-social.svg
      target: http://travis-ci.org/noirbizarre/pelican-social
      alt: pelican-social Travis CI badge
   -  image: https://img.shields.io/coveralls/noirbizarre/pelican-social.svg
      target: https://coveralls.io/r/noirbizarre/pelican-social
      alt: pelican-social Coveralls badge
   -  image: https://img.shields.io/pypi/v/pelican-social.svg
      target: https://pypi.python.org/pypi/pelican-social
      alt: pelican-social last version badge
   -  image: https://img.shields.io/pypi/dm/pelican-social.svg
      target: https://pypi.python.org/pypi/pelican-social
      alt: pelican-social downloads badge
---

Un plugin pour [Pelican](http://getpelican.com/), le générateur de blog static,
permettant d'insérer facilement des liens vers différents réseaux sociaux.
