---
name: pelican-microdata
url: https://github.com/noirbizarre/pelican-microdata

started: 2013-03-25

badges:
   -  image: https://secure.travis-ci.org/noirbizarre/pelican-microdata.svg
      target: http://travis-ci.org/noirbizarre/pelican-microdata
      alt: pelican-microdata Travis CI badge
   -  image: https://img.shields.io/coveralls/noirbizarre/pelican-microdata.svg
      target: https://coveralls.io/r/noirbizarre/pelican-microdata
      alt: pelican-microdata Coveralls badge
   -  image: https://img.shields.io/pypi/v/pelican-microdata.svg
      target: https://pypi.python.org/pypi/pelican-microdata
      alt: pelican-microdata last version badge
   -  image: https://img.shields.io/pypi/dm/pelican-microdata.svg
      target: https://pypi.python.org/pypi/pelican-microdata
      alt: pelican-microdata downloads badge

---

Un plugin pour [Pelican](http://getpelican.com/), le générateur de blog static,
permettant d'insérer des marqueurs sémantiques [Microdata](http://schema.org/)
dans le contenu.
