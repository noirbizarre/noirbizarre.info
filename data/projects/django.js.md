---
name: django.js
url: https://github.com/noirbizarre/django.js

started: 2012-09-01

badges:
   -  image: https://secure.travis-ci.org/noirbizarre/django.js.png
      target: http://travis-ci.org/noirbizarre/django.js
      alt: Django.js Travis CI badge
   -  image: https://img.shields.io/coveralls/noirbizarre/django.js.svg
      target: https://coveralls.io/r/noirbizarre/django.js
      alt: Django.js Coveralls badge
   -  image: https://img.shields.io/pypi/v/django.js.svg
      target: https://pypi.python.org/pypi/django.js
      alt: Django.js last version badge
   -  image: https://img.shields.io/pypi/dm/django.js.svg
      target: https://pypi.python.org/pypi/django.js
      alt: Django.js downloads badge
   -  image: https://readthedocs.org/projects/djangojs/badge/?version=stable
      target: https://djangojs.readthedocs.io/en/latest/
      alt: Documentation

---

Application Django facilitant l’utilisation du JavaScript
(reverse URL et context côté client, outils de tests, template tags...).
