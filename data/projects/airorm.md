---
name: airorm
url: https://github.com/noirbizarre/airorm

started: 2008-10-01
---

Un ORM en ActionScript 3 sur le pattern ActiveRecord pour la base de données embarquées d’Adobe Air.
Aujourd'hui, je n'utilise ni ne maintient plus ce projet.
