---
name: ezWall
url: https://github.com/noirbizarre/ezwall-plugin

started: 2012-02-14
---

Un plugin Jenkins fournissant un buildwall pour toutes les vues. Le plugin utilise HTML5, CSS3 et Backbone.js
