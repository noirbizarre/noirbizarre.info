---
name: description-column-plugin
url: https://wiki.jenkins-ci.org/display/JENKINS/Description+Column+Plugin

started: 2012-11-01
---

Un simple plugin Jenkins ajoutant le champs description dans la liste des jobs.
Ce plugin a été intégré à
[Extra Columns Plugin](https://wiki.jenkins-ci.org/display/JENKINS/Extra+Columns+Plugin)
