---
name: gonja
url: https://github.com/noirbizarre/gonja

started: 2019-04-10

badges:
  - image: https://secure.travis-ci.org/noirbizarre/gonja.svg?branch=master
    target: http://travis-ci.org/noirbizarre/gonja
    alt: gonja Travis CI badge
  - image: https://godoc.org/github.com/noirbizarre/gonja?status.svg
    target: https://godoc.org/github.com/noirbizarre/gonja
    alt: Documentation
  - image: https://codecov.io/gh/noirbizarre/gonja/branch/master/graph/badge.svg
    target: https://codecov.io/gh/noirbizarre/gonja
    alt: gonja codecov badge
---

Gonja est un moteur de template en go utilisant la syntaxe Jinja et ayant pour objectif d'être 100% compatible.
