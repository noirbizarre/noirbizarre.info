---
name: pelican-data
url: https://github.com/noirbizarre/pelican-data

started: 2016-06-01

badges:
   -  image: https://secure.travis-ci.org/noirbizarre/pelican-data.png
      target: http://travis-ci.org/noirbizarre/pelican-data
      alt: Travis-CI build status
   -  image: https://img.shields.io/coveralls/noirbizarre/pelican-data.svg
      target: https://coveralls.io/r/noirbizarre/pelican-data
      alt: Coverage
   -  image: https://img.shields.io/pypi/v/pelican-data.svg
      target: https://pypi.python.org/pypi/pelican-data
      alt: Last published version on PyPI
   -  image: https://img.shields.io/pypi/dm/pelican-data.svg
      target: https://pypi.python.org/pypi/pelican-data
      alt: Monthly donwloads on PyPI
---

Un plugin pour [Pelican](http://getpelican.com/), le générateur de blog static,
ajout le support des données sous formes de fichiers JSON ou YAML,
ainsi que des collections.
