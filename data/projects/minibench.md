---
name: MiniBench
url: https://github.com/noirbizarre/minibench

started: 2015-11-01

badges:
  - image: https://secure.travis-ci.org/noirbizarre/minibench.png
    target: http://travis-ci.org/noirbizarre/minibench
    alt: Travis CI build status
  - image: https://img.shields.io/coveralls/noirbizarre/minibench.svg
    target: https://coveralls.io/r/noirbizarre/minibench
    alt: Coveralls coverage
  - image: https://img.shields.io/pypi/v/minibench.svg
    target: https://pypi.python.org/pypi/minibench
    alt: Last version badge
  - image: https://img.shields.io/pypi/l/minibench.svg
    target: https://pypi.python.org/pypi/minibench
    alt: Minibench License badge
  - image: https://img.shields.io/pypi/pyversions/minibench.svg
    target: https://pypi.python.org/pypi/minibench
    alt: Minibench Python versions badge
  - image: https://readthedocs.org/projects/minibench/badge/?version=stable
    target: https://minibench.readthedocs.io/en/stable/
    alt: Documentation

---

MiniBench provides a simple framework for benchmarking following the unittest module pattern.
