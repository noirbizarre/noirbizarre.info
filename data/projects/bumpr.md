---
name: Bump'R
url: https://github.com/noirbizarre/bumpr

started: 2013-08-20

badges:
  - image: https://secure.travis-ci.org/noirbizarre/bumpr.svg?branch=master
    target: http://travis-ci.org/noirbizarre/bumpr
    alt: bumpr Travis CI badge
  - image: https://readthedocs.org/projects/bumpr/badge/?version=stable
    target: https://bumpr.readthedocs.io/en/latest/
    alt: Documentation
  - image: https://coveralls.io/repos/github/noirbizarre/bumpr/badge.svg?branch=master
    target: https://coveralls.io/r/noirbizarre/bumpr
    alt: bumpr Coveralls badge
  - image: https://img.shields.io/pypi/v/bumpr.svg
    target: https://pypi.python.org/pypi/bumpr
    alt: Bump'R last version badge
  - image: https://img.shields.io/pypi/l/bumpr.svg
    target: https://pypi.python.org/pypi/bumpr
    alt: Bump'R License badge
  - image: https://img.shields.io/pypi/pyversions/bumpr.svg
    target: https://pypi.python.org/pypi/bumpr
    alt: Bump'R Python versions badge
---

Bump'R est un outil permettant d'effectuer des releases propres en une seule commande
en fournissant un workflow simple et extensible.
