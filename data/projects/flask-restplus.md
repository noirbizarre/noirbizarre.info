---
name: Flask RESTPlus
slug: flask-restplus
url: https://github.com/noirbizarre/flask-restplus

started: 2014-08-01

badges:
  - image: https://secure.travis-ci.org/noirbizarre/flask-restplus.svg?branch=master
    target: http://travis-ci.org/noirbizarre/flask-restplus
    alt: flask-restplus Travis CI badge
  - image: https://readthedocs.org/projects/flask-restplus/badge/?version=stable
    target: https://flask-restplus.readthedocs.io/en/latest/
    alt: Documentation
  - image: https://coveralls.io/repos/github/noirbizarre/flask-restplus/badge.svg?branch=master
    target: https://coveralls.io/r/noirbizarre/flask-restplus
    alt: flask-restplus Coveralls badge
  - image: https://img.shields.io/pypi/v/flask-restplus.svg
    target: https://pypi.python.org/pypi/flask-restplus
    alt: Flask RESTPlus last version badge
  - image: https://img.shields.io/pypi/l/flask-restplus.svg
    target: https://pypi.python.org/pypi/flask-restplus
    alt: Flask RESTPlus License badge
  - image: https://img.shields.io/pypi/pyversions/flask-restplus.svg
    target: https://pypi.python.org/pypi/flask-restplus
    alt: Flask RESTPlus Python versions badge
---

Ecrivez des API propres, docuements repidement avec Flask.
