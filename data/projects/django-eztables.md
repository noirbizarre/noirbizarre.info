---
name: django-eztables
url: https://github.com/noirbizarre/django-eztables

started: 2013-02-01

badges:
   -  image: https://secure.travis-ci.org/noirbizarre/django-eztables.png
      target: http://travis-ci.org/noirbizarre/django-eztables
      alt: django-eztables Travis CI badge
   -  image: https://img.shields.io/coveralls/noirbizarre/django-eztables.svg
      target: https://coveralls.io/r/noirbizarre/django-eztables
      alt: django-eztables Coveralls badge
   -  image: https://img.shields.io/pypi/v/django-eztables.svg
      target: https://pypi.python.org/pypi/django-eztables
      alt: django-eztables last version badge
   -  image: https://img.shields.io/pypi/dm/django-eztables.svg
      target: https://pypi.python.org/pypi/django-eztables
      alt: django-eztables downloads badge
---

Une application Django pour faciliter la création des tableaux avec jQuery DataTables.
