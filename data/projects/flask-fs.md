---
name: Flask FS
slug: flask-fs
url: https://github.com/noirbizarre/flask-fs

started: 2014-10-01

badges:
  - image: https://secure.travis-ci.org/noirbizarre/flask-fs.svg?branch=master
    target: http://travis-ci.org/noirbizarre/flask-fs
    alt: flask-fs Travis CI badge
  - image: https://readthedocs.org/projects/flask-fs/badge/?version=stable
    target: https://flask-fs.readthedocs.io/en/latest/
    alt: Documentation
  - image: https://coveralls.io/repos/github/noirbizarre/flask-fs/badge.svg?branch=master
    target: https://coveralls.io/r/noirbizarre/flask-fs
    alt: flask-fs Coveralls badge
  - image: https://img.shields.io/pypi/v/flask-fs.svg
    target: https://pypi.python.org/pypi/flask-fs
    alt: Flask FS last version badge
  - image: https://img.shields.io/pypi/l/flask-fs.svg
    target: https://pypi.python.org/pypi/flask-fs
    alt: Flask FS License badge
  - image: https://img.shields.io/pypi/pyversions/flask-fs.svg
    target: https://pypi.python.org/pypi/flask-fs
    alt: Flask FS Python versions badge

---

Simple and easy file storages for Flask.

Flask FS is a Django-inspired storages managers with multiple backend support
(Filesystem, Amazon S3, Swift, GridFS)
