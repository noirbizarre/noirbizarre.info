---
name: pelican-frontmark
url: https://github.com/noirbizarre/pelican-frontmark

started: 2016-06-01

badges:
  - image: https://secure.travis-ci.org/noirbizarre/pelican-frontmark.svg
    target: http://travis-ci.org/noirbizarre/pelican-frontmark
    alt: Travis-CI build status
  - image: https://img.shields.io/coveralls/noirbizarre/pelican-frontmark.svg
    target: https://coveralls.io/r/noirbizarre/pelican-frontmark
    alt: Coverage
  - image: https://img.shields.io/pypi/v/pelican-frontmark.svg
    target: https://pypi.python.org/pypi/pelican-frontmark
    alt: Last published version on PyPI
  - image: https://img.shields.io/pypi/l/pelican-frontmark.svg
    target: https://pypi.python.org/pypi/pelican-frontmark
    alt: Pelican FrontMark License badge
  - image: https://img.shields.io/pypi/pyversions/pelican-frontmark.svg
    target: https://pypi.python.org/pypi/pelican-frontmark
    alt: Pelican FrontMark Python versions badge
---

Un plugin pour [Pelican](http://getpelican.com/), le générateur de blog static,
permettant de rediger les contenus en utilisant [CommonMark](http://commonmark.org/)
avec une entête [Front Matter](https://jekyllrb.com/docs/frontmatter/) pour les metadonnées.
