---
name: udata
url: https://github.com/opendatateam/udata

started: 2013-08-20

badges:
  - image: https://circleci.com/gh/opendatateam/udata.svg?style=shield
    target: https://circleci.com/gh/opendatateam/udata
    alt: udata CircleCI badge
  - image: https://readthedocs.org/projects/udata/badge/?version=stable
    target: https://udata.readthedocs.io/en/latest/
    alt: Documentation
  - image: https://img.shields.io/pypi/v/udata.svg
    target: https://pypi.python.org/pypi/udata
    alt: udata last version badge
  - image: https://img.shields.io/pypi/l/udata.svg
    target: https://pypi.python.org/pypi/udata
    alt: udata License badge
  - image: https://img.shields.io/pypi/pyversions/udata.svg
    target: https://pypi.python.org/pypi/udata
    alt: udata Python versions badge
---

[![logo](https://i.imgur.com/rlRox1c.png)](https://github.com/opendatateam/udata)

`udata` est un socle technique libre de portail Open Data.
C'est le projet qui me rémunère depuis 2013.
