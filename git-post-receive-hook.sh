#!/bin/sh

STAGING="/home/noirbizarre/staging/blog"

unset GIT_DIR
cd $STAGING
git pull
git submodule update --init
./publish.sh
