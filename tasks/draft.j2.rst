{{ title }}
{{ '#' * title|length }}

:date: {{ date.strftime('%Y-%m-%d %H:%M') }}
:modified: {{ date.strftime('%Y-%m-%d %H:%M') }}
:tags: {{ tags }}
:category: {{ category }}
:slug: {{ slug }}
:authors: Axel Haustant
:summary: {{ summary }}
:status: draft
