import locale
import logging
import os
import shutil
import sys

from datetime import datetime

from invoke import task

from jinja2 import Environment, FileSystemLoader
from pelican import Pelican, log
from pelican.settings import read_settings

try:
    from importlib import reload
except:
    from imp import reload


#: Project absolute root path
TASKS_ROOT = os.path.dirname(__file__)
ROOT = os.path.abspath(os.path.join(TASKS_ROOT, '..'))
THEMES_ROOT = os.path.join(ROOT, 'themes')

CONF_FILE = os.path.join(ROOT, 'pelicanconf.py')

THEME = 'Flex'
THEME_DIR = os.path.join(THEMES_ROOT, THEME)

# Port for `serve`
PORT = 5000


class objdict(dict):
    def __getattr__(self, name):
        return self[name]


def get_settings():
    return objdict(read_settings(CONF_FILE))


jinja_env = Environment(loader=FileSystemLoader(TASKS_ROOT))


def jinja(template, filename, **ctx):
    template = jinja_env.get_template(template)
    with open(filename, 'wb') as out:
        data = template.render(**ctx)
        out.write(data.encode('utf-8'))


def prompt(text):
    encoding = sys.stdin.encoding or locale.getpreferredencoding(True)
    return raw_input(text).decode(encoding)


@task
def clean(ctx):
    '''Remove generated files'''
    settings = get_settings()
    if os.path.isdir(settings.OUTPUT_PATH):
        shutil.rmtree(settings.OUTPUT_PATH)
        os.makedirs(settings.OUTPUT_PATH)


@task
def build(ctx, verbose=False, debug=False):
    '''Build local version of site'''
    cmd = 'pelican -s publishconf.py'.split(' ')
    if verbose:
        cmd.append('-v')
    if debug:
        cmd.append('-D')
    with ctx.cd(ROOT):
        ctx.run(' '.join(cmd))


def draft(article=False):
    '''Create a draft page'''
    import slugify

    title = input('Title: ')
    slug = slugify.slugify(title, to_lower=True)
    name = slugify.slugify(title, to_lower=True)
    slug = input('Slug ({0}): '.format(slug)) or slug
    name = input('Name ({0}): '.format(name)) or name
    lang = input('Language ({0}): '.format('fr')) or 'fr'
    summary = input('Summary: ')
    tags = [t for t in input('Tags: ').split(',') if t]
    category = input('Category: ') if article else None
    now = datetime.now()
    if article:
        filename = '{0}-{1}.md'.format(now.date().isoformat(), slug)
        filename = os.path.join('articles', lang, filename)
    else:
        filename = os.path.join('pages', lang, '{0}.md'.format(slug))
    os.makedirs(os.path.dirname(filename), exist_ok=True)
    jinja('draft.j2.md', filename,
          title=title,
          slug=slug,
          name=name,
          category=category,
          summary=summary,
          tags=tags,
          lang=lang,
          is_article=article,
          date=now)


@task
def page(ctx):
    '''Create a draft page'''
    draft(article=False)


@task
def article(ctx):
    '''Create a draft article'''
    draft(article=True)


@task
def i18n(ctx):
    '''Extract theme translatable strings'''
    with ctx.cd(THEME_DIR):
        ctx.run('pybabel extract -F translations/babel.cfg -o translations/messages.pot .')
        ctx.run('pybabel update -i translations/messages.pot -d translations -l fr')


@task
def i18nc(ctx):
    '''Compile theme translations'''
    with ctx.cd(THEME_DIR):
        ctx.run('pybabel compile -d translations')


def reload_and_compile():
    _sys_path = sys.path[:]
    settings = get_settings()
    for pluginpath in settings.PLUGIN_PATHS:
        sys.path.insert(0, pluginpath)
    for name, module in sys.modules.items():
        root_module = name.split('.', 1)[0]
        if root_module in settings.PLUGINS:
            reload(module)

    sys.path = _sys_path
    compile()


def compile():
    settings = get_settings()
    p = Pelican(settings)
    try:
        p.run()
    except SystemExit as e:
        pass


@task
def watch(ctx, verbose=False):
    '''Serve the blog and watch changes'''
    from livereload import Server

    settings = get_settings()

    log.init(logging.DEBUG if verbose else logging.INFO)
    logging.getLogger('livereload').propagate = False
    logging.getLogger('tornado').propagate = False

    compile()
    server = Server()
    server.watch(CONF_FILE, compile)

    server.watch(os.path.join(ROOT, 'themes'), compile)
    server.watch(os.path.join(ROOT, 'local_plugins'), reload_and_compile)
    server.watch(os.path.join(ROOT, 'extras'), compile)

    DATA_PATHS = getattr(settings, 'DATA_PATHS', [])
    for lang in settings.I18N_SUBSITES.keys():
        DATA_PATHS.extend(settings.I18N_SUBSITES[lang].get('DATA_PATHS', []))

    for root in set(DATA_PATHS):
        for data in getattr(settings, 'DATA', []):
            path = os.path.join(ROOT, root, data)
            if os.path.exists(path):
                server.watch(path, compile)

    paths = settings.ARTICLE_PATHS + settings.PAGE_PATHS + settings.STATIC_PATHS
    for path in paths:
        server.watch(os.path.join(ROOT, path), compile)

    server.serve(port=PORT, root=settings.OUTPUT_PATH)
