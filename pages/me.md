---
title: Qui suis-je ?
---

Bonjour, je m'appelle *Axel Haustant* alias *noirbizarre* et moi aussi je suis un GeeK !

Je suis *développeur* Python et JavaScript et passionné d'informatique depuis mon plus jeune âge!

Quand je ne travail pas, je passe du temps sur mes [projets persos][],
mais j'ai quand même une vie à côté, que je répartie entre:

- mes proches
- le sport
- les séries
- la bonne nourriture
- les balades en nature

Si vous voulez en savoir plus sur mon parcour professionel:

- [CV en ligne](http://axel.haustant.fr)
- [online resume](http://axel.haustant.fr/en)

Vous pouvez me retrouver sur différents sites sous le pseudo noirbizarre.


[projets persos]: {filename}/pages/projects.md
