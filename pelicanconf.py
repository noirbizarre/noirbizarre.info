#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from datetime import datetime
from os.path import join, dirname
from pelican import __version__ as PELICAN_VERSION

AUTHOR = 'Axel Haustant'
SITENAME = 'noirbizarre.info'
SITESUBTITLE = 'Encore un blog de GeeK'
SITEURL = ''
SITELOGO = SITEURL + '/images/me/mangavatar-2.png'
RELATIVE_URLS = True

THEME = 'themes/Flex'

PYGMENTS_STYLE = 'solarized-dark'
FRONTMARK_PYGMENTS = True

PATH = dirname(__file__)
OUTPUT_PATH = join(PATH, 'output')
ARTICLE_PATHS = [
    'articles',
]

TEMPLATE_PAGES = {
    'extras/404.html': '404.html',
    'extras/drafts.html': 'drafts/index.html',
}

TIMEZONE = 'Europe/Paris'

TYPOGRIFY = True

DEFAULT_LANG = 'fr'
DEFAULT_DATE_FORMAT = '%d %B %Y'

# Blogroll
LINKS = (
    ('API Hackers', 'https://apihackers.com/'),
    ('Un blog sur la banquise', 'http://www.rcommande.org/'),
)

# Social widget
SOCIAL = (
    ('github', 'https://github.com/noirbizarre'),
    ('twitter', 'https://twitter.com/noirbizarre'),
    # ('google+', 'https://plus.google.com/118323681296003594129?rel=author'),
    ('linkedin', 'https://fr.linkedin.com/in/axelhaustant/'),
    ('rss', 'https://noirbizarre.info/atom'),
    # ('delicious', 'http://delicious.com/noirbizarre'),
    # ('lastfm', 'http://lastfm.com/user/noirbizarre'),
    # ('email', 'mailto:noirbizarre@gmail.com'),
)

SUMMARY_MAX_LENGTH = 100
DEFAULT_PAGINATION = 5
TAG_CLOUD_STEPS = 8
TAG_CLOUD_MAX_ITEMS = 35

STATIC_PATHS = [
    'images',
    'extras/keybase.txt',
    'extras/BingSiteAuth.xml',
    'extras/custom.css',
    'extras/robots.txt',
]


# Pretty URLS
PAGE_URL = '{slug}/'
PAGE_SAVE_AS = '{slug}/index.html'

ARTICLE_URL = '{date:%Y}/{date:%m}/{date:%d}/{slug}/'
ARTICLE_SAVE_AS = '{date:%Y}/{date:%m}/{date:%d}/{slug}/index.html'

CATEGORY_URL = 'category/{slug}/'
CATEGORY_SAVE_AS = 'category/{slug}/index.html'

TAG_URL = 'tag/{slug}/'
TAG_SAVE_AS = 'tag/{slug}/index.html'

TAGS_URl = 'tags/'
TAGS_SAVE_AS = 'tags/index.html'

AUTHOR_URL = 'authors/{slug}/'
AUTHOR_SAVE_AS = 'authors/{slug}/index.html'

ARCHIVES_URL = 'archvives/'
ARCHIVES_SAVE_AS = 'archvives/index.html'

# Feed generation is usually not desired when developing
# FEED_ALL_ATOM = None
# CATEGORY_FEED_ATOM = None
# TRANSLATION_FEED_ATOM = None
# AUTHOR_FEED_ATOM = None
# AUTHOR_FEED_RSS = None

FEED_ALL_ATOM = 'feeds/all.atom'
# FEED_ALL_RSS = 'rss'
CATEGORY_FEED_ATOM = 'category/%s/atom'
# CATEGORY_FEED_RSS = 'category/%s/rss'

PLUGIN_PATHS = (
    'local_plugins',
    'plugins',
)

PLUGINS = (
    'neighbors',
    'sitemap',
    'microdata',
    'social',
    'code_include',
    'tag_cloud',
    'frontmark',
    'data',
    'i18n_subsites',
    'post_stats',
)

DATA_PATHS = ['data']

DATA = (
    'projects',
)


# SEO
METAS = {
    'author': 'Axel Haustant',
    'description': "Le blog d'un développeur qui veut partager ses connaissances et expériences",
    'keywords': 'linux, python, django, javascript, java, system',
    'generator': 'Pelican %s' % PELICAN_VERSION,
}

PERSON_METAS = {
    'name': 'Axel Haustant',
    'nickname': 'noirbizarre',
    'title': 'développeur',
    'role': 'consultant',
    'affiliation': 'Etalab',
}

SITEMAP = {
    'format': 'xml',
    'priorities': {
        'articles': 0.5,
        'indexes': 0.5,
        'pages': 0.5
    },
    'changefreqs': {
        'articles': 'daily',
        'indexes': 'daily',
        'pages': 'monthly'
    }
}

# Jinja Configuration
JINJA_ENVIRONMENT = {'extensions': ['jinja2.ext.i18n']}
I18N_TEMPLATES_LANG = 'en'
I18N_GETTEXT_NEWSTYLE = True

LANGUAGES = {
    'en': 'English',
    'fr': 'Français',
}


EXTRA_PATH_METADATA = {
    'extras/custom.css': {'path': 'static/custom.css'},
    'extras/robots.txt': {'path': 'robots.txt'},
    'extras/keybase.txt': {'path': 'keybase.txt'},
    'extras/BingSiteAuth.xml': {'path': 'BingSiteAuth.xml'},
}


# Flex theme customization
MAIN_MENU = True
CUSTOM_CSS = 'static/custom.css'
COPYRIGHT_YEAR = datetime.now().year
BROWSER_COLOR = '#002b36'
DISABLE_URL_HASH = True
SHOW_DRAFTS = True

# i18n subsites
I18N_SUBSITES = {
#     'en': {
#         'MENUS': from_yaml('en/menus'),
#     },
}
