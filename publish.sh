#!/bin/sh

VENV="venv"
PUBLIC="/home/noirbizarre/www/noirbizarre.info/blog"
SITEMAP=http://noirbizarre.info/sitemap.xml.gz

# Setup virtualenv
if [ ! -d "$VENV" ]; then
    virtualenv --distribute $VENV
fi
. $VENV/bin/activate
pip install -r requirements.pip

# Deploy blog
pelican -v -o $PUBLIC -s publishconf.py
cp -f nginx.conf /etc/nginx/sites-available/noirbizarre.info
sudo service nginx reload

# Ping sitemap
GOOGLE=http://www.google.com/webmasters/tools/ping?sitemap=$SITEMAP
BING=http://www.bing.com/webmaster/ping.aspx?siteMap=$SITEMAP
# ASK=http://submissions.ask.com/ping?sitemap=$SITEMAP

for url in $GOOGLE $BING; do
    curl -s -w "%{http_code} %{url_effective}\\n" "$url" -o /dev/null
done
