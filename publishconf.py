#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
sys.path.append('.')
from pelicanconf import *

SITEURL = 'https://noirbizarre.info'
FEED_DOMAIN = SITEURL

# URLs
# ARTICLE_URL = '{date:%Y}/{date:%m}/{date:%d}/{slug}/'
# CATEGORY_URL = 'category/{slug}/'
# TAG_URL = 'tag/{slug}/'
# PAGE_URL = 'pages/{slug}/'

PLUGINS += (
    # 'optimize_images',
    'image_optimizer',
    'gzip_cache',
)

DELETE_OUTPUT_DIRECTORY = True
IMAGE_OPTIMIZATION_ONCE_AND_FOR_ALL = False

# Following items are often useful when publishing

# Uncomment following line for absolute URLs in production:
RELATIVE_URLS = False

TWITTER_USERNAME = 'noirbizarre'
MYOPENID_USERNAME = 'noirbizarre'
DISQUS_SITENAME = "noirbizarre"
FLATTR_USERNAME = "noirbizarre"
FLATTR_LANG = "fr_FR"
PLUS_ONE = True
PLUS_ONE_LANG = 'fr'
GOOGLE_ANALYTICS = 'UA-6573533-1'
SHOW_DRAFTS = False
