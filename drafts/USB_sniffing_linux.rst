USB Sniffing sous linux
#######################
:date: 2010-10-03 18:16
:author: Noirbizarre
:category: Développement
:tags: kernel, linux, sniffing, usb, usbmon, virtualbox

Lors du développement de drivers pour certains matériels, il arrive de
ne pas avoir accès aux spécifications et de devoir faire du reverse
engeneering pour les retrouver. Nous allons voir comment faire pour
faire le reverse engegeering d'un périphérique USB (dans mon cas un
adapteur X10 CM15 Pro) en utilisant le logiciel qui est fourni avec sous
Windows dans VirtualBox et en écoutant les communications USB (USB
Sniffing) depuis Linux.

Principe
--------

.. raw:: html

   </p>

Le noyau Linux fourni un

﻿

.. raw:: html

   <p>

::

    $> lsusbBus 007 Device 001: ID 1d6b:0001 Linux Foundation 1.1 root hubBus 006 Device 006: ID 0bc7:0001 X10 Wireless Technology, Inc. ActiveHome (ACPI-compliant)Bus 006 Device 001: ID 1d6b:0001 Linux Foundation 1.1 root hubBus 005 Device 001: ID 1d6b:0001 Linux Foundation 1.1 root hubBus 004 Device 002: ID 0b05:1712 ASUSTek Computer, Inc. BT-183 Bluetooth 2.0+EDR adapterBus 004 Device 001: ID 1d6b:0001 Linux Foundation 1.1 root hubBus 003 Device 001: ID 1d6b:0001 Linux Foundation 1.1 root hubBus 002 Device 002: ID 04f2:b012 Chicony Electronics Co., Ltd 1.3 MPixel UVC WebcamBus 002 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hubBus 001 Device 002: ID 0bda:0158 Realtek Semiconductor Corp. Mass Storage DeviceBus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub

.. raw:: html

   </p>

