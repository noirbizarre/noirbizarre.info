Install postfix-pcre

/etc/postfix/virtual

/^((?!dev\+).*)@(.*)$/  dev+$1-at-$2@mydomain.com


/etc/postfix/main.cf

virtual_alias_maps = pcre:/etc/postfix/virtual
and have tested it with
Code:

Test:

postmap -q test@some.domain pcre:/etc/postfix/virtual
dev+test-at-some.domain@mydomain.com


service postfix restart
