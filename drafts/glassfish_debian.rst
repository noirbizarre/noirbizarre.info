Installation de GlassFish V3 sous Debian Lenny
##############################################
:date: 2011-01-07 00:35
:author: Noirbizarre
:category: Admin
:tags: debian, glassfish, java, openjdk

Dans cet article décrit l'installation de GlassFish V3 sur Debian Lenny.

Installation d'OpenJDK 6 Headless
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. raw:: html

   </p>

Avant toute chose, il faut une installation de JRE fonctionnelle, dans
mon cas OpenJDK 6 Headless.

[code light="true"]

noirbizarre:~$ sudo aptitude install openjdk-6-jre-headless

[/code]

Création utilisateur
~~~~~~~~~~~~~~~~~~~~

.. raw:: html

   </p>

GlassFish

[code light="true"]

noirbizarre:~$ sudo groupadd glassfish

noirbizarre:~$ sudo useradd -s /bin/bash -d /home/glassfish -m -g
glassfish glassfish

[/code]

Installation de GlassFish dans son $HOME
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. raw:: html

   </p>

[code light="true"]

noirbizarre:~$ sudo passwd glassfish

noirbizarre:~$ sudo -i -u glassfish

glassfish:~$ wget
http://download.java.net/glassfish/3.0.1/release/glassfish-3.0.1-web-ml.zip

glassfish:~$ unzip glassfish-3.0.1-web-ml.zip

glassfish:~$ exit

[/code]

Installation en tant que service
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. raw:: html

   </p>

[bash title="/etc/init.d/glassfish"]

#!/bin/sh

#

# start/stop GlashFish application server

GF\_USER=glassfish

GF\_HOME=/home/$GF\_USER/glassfishv3/glassfish

ASADMIN=$GF\_HOME/bin/asadmin

SU="su --login $GF\_USER --command "

case "$1" in

start)

$SU "$ASADMIN start-domain > /dev/null 2>&1 &"

;;

stop)

$SU "$ASADMIN stop-domain > /dev/null 2>&1 &"

;;

restart)

$SU "$ASADMIN restart-domain > /dev/null 2>&1 &"

;;

\*)

echo "usage: $0 (start\|stop\|restart\|help)"

esac

[/bash]

[code light="true"]

noirbizarre:~$ sudo chmod +x /etc/init.d/glassfish

noirbizarre:~$ sudo update-rc.d glassfish defaults

[/code]

[code light="true"]

noirbizarre:~$ sudo /etc/init.d/glassfish start\|stop\|restart

[/code]
