Dépôts Git: découpage, refactoring et nettoyage
###############################################
:date: 2012-06-04 14:44
:author: Noirbizarre
:category: Développement
:tags: git

git clone repo.git

cd repo

# get needed branch

for i in branch1 br2 br3; do git branch -t $i origin/$i; done

# Prepare Layout

git mv ...

git rm ...

git commit ...

# Remove useless tags

git tag -l \| xargs git tag -d

# Remove origin repository

git remote rm origin

# Filter branch on a subdirectory

git filter-branch --prune-empty -f --subdirectory-filter src -- --all

# Remove extra folders from history

git filter-branch --prune-empty -f --tree-filter 'rm -rf folder' --
--all

# Remove extra files from history

git filter-branch --prune-empty -f --index-filter 'git update-index
--remove file.html' -- --all

#Fix CRLF

git filter-branch --prune-empty -f --tree-filter 'fix-line-endings.sh'
-- --all

#fix-line-endings.sh:

#!/bin/sh

find . -type f -a \\(-name '\*.py' -o -name '\*.js' -o -name '\*.css' -o
-name '\*.sh' -o -name '\*.txt' -o -name '\*.html' \\) \| xargs fromdos

# Clean up and optimize repository metadatas

git reset --hard

git for-each-ref --format="%(refname)" refs/original/ \| xargs -n 1 git
update-ref -d

git reflog expire --expire=now --all

git gc --aggressive --prune=now

# Fix some configurations to handle new layout

git ci --amend ...

git ci ...

# Deploy the new repository

git remote add origin ssh://remote/origin.git

git push origin master
