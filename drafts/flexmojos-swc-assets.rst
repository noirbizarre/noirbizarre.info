Flexmojos, swc and Assets
#########################
:date: 2011-09-12 11:16
:author: Noirbizarre
:category: Non classé

J'ai souvent eu besoin de partager des assets entre plusieurs projets
Flex en utilisant un SWC partagé.

La documentation de Flexmojos n'est pas très claire sur le sujet mais je
suis arrivé à 2 solutions différentes mais fonctionnelles.

Solution 1: spécifier les assets comme inclus lors de la compilation
--------------------------------------------------------------------

.. raw:: html

   </p>

Par défaut, lors de l'execution de flexmojos:compile-swc, toutes les
classes sont compilées et incluse dans le SWC mais aucun autre fichier
n'est rajouté.

Il faut alors spécifier au compilateur la liste des fichiers à inclure
en plus des classes publiées.
