Flexmojos, swc et assets
########################
:date: 2011-09-12 11:46
:author: Noirbizarre
:category: Flex
:tags: assets, flex, flexmojos, swf

J'ai souvent eu besoin de partager des assets entre plusieurs projets
Flex en utilisant un SWC partagé. La documentation de Flexmojos n'est
pas très claire sur le sujet mais je suis arrivé à 2 solutions
différentes mais fonctionnelles.

Solution 1: spécifier les assets comme inclus lors de la compilation
--------------------------------------------------------------------

.. raw:: html

   </p>

Par défaut, lors de l'execution de flexmojos:compile-swc, toutes les
classes sont compilées et incluse dans le SWC mais aucun autre fichier
n'est rajouté. Il faut alors spécifier au compilateur la liste des
fichiers à inclure en plus des classes publiées.

Solution 2: liés les assets dans une classe
-------------------------------------------

.. raw:: html

   </p>

C'est la solution que je préfère car plus propres. Il n'est plus
nécéssaire de lister les assets à intégrer dans le pom.xml.
