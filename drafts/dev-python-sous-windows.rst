Environnement de développement Python sous Windows
##################################################
:date: 2011-08-16 11:06
:author: Noirbizarre
:category: Python
:tags: msysgit, pydev, python, setuptools, virtualenv, windows

L'objectif de cet article est de créer un environnement de développement
Python sous Windows en utilisant MSYS comme shell.

Installation de base
--------------------

.. raw:: html

   </p>

Récupérer la dernière version sur http://www.python.org et lancer
l'installeur (dans notre cas python-2.7.1.amd64.msi). Modifier le
``PATH`` pour y ajouter ``C:\Python27;C:\Python27\Scripts``

Si l'installation se passe sous un Windows 64 bits, il faut insérer le
fichier .reg suivant (exemple avec Python 2.7.1):

[sourcecode light="true"]

Windows Registry Editor Version 5.00

[HKEY\_LOCAL\_MACHINE\\SOFTWARE\\Wow6432Node\\Python]

[HKEY\_LOCAL\_MACHINE\\SOFTWARE\\Wow6432Node\\Python\\PythonCore]

[HKEY\_LOCAL\_MACHINE\\SOFTWARE\\Wow6432Node\\Python\\PythonCore\\2.7]

[HKEY\_LOCAL\_MACHINE\\SOFTWARE\\Wow6432Node\\Python\\PythonCore\\2.7\\Help]

[HKEY\_LOCAL\_MACHINE\\SOFTWARE\\Wow6432Node\\Python\\PythonCore\\2.7\\Help\\Main
Python Documentation]

@="C:\\\\Python27\\\\Doc\\\\python271.chm"

[HKEY\_LOCAL\_MACHINE\\SOFTWARE\\Wow6432Node\\Python\\PythonCore\\2.7\\InstallPath]

@="C:\\\\Python27\\\\"

[HKEY\_LOCAL\_MACHINE\\SOFTWARE\\Wow6432Node\\Python\\PythonCore\\2.7\\InstallPath\\InstallGroup]

@="Python 2.7"

[HKEY\_LOCAL\_MACHINE\\SOFTWARE\\Wow6432Node\\Python\\PythonCore\\2.7\\Modules]

[HKEY\_LOCAL\_MACHINE\\SOFTWARE\\Wow6432Node\\Python\\PythonCore\\2.7\\PythonPath]

@="C:\\\\Python27\\\\Lib;C:\\\\Python27\\\\DLLs"

[/sourcecode]

Cette modification va permettre, entre autres, de permettre la détection
de l'installation de Python par les extension suivantes.

Installer pywin32 Récupérer la dernière version `ici`_ (dans notre cas
pywin32-216.win-amd64-py2.7.exe)

Installer SetupTools Dernière version `ici`_. Dans notre cas
`setuptools-0.6c11.win32-py2.7.exe`_

Installer virtualenv depuis la ligne de commande:

[sourcecode light="true"]$ easy\_install virtualenv[/sourcecode]

msys et virtualenvwrapper
-------------------------

.. raw:: html

   </p>

Pour simplifier l'utilisation quotidienne du shell et de virtualenv,
nous allons utiliser le shell msys et virtualenvwrapper. Comme j'utilise
git sous Windows via msysgit, je réutiliserai cette installation, que je
ne détaillerai pas. virtualenvwrapper est un script shell qui simplifie
l'utilisation de virtualenv. msysgit nous fournissans le shell, nous
pouvons y installer virtualenvwrapper (version 2.8 minimum pour le
support de MSYS):

[sourcecode light="true"]$ easy\_install virtualenvwrapper[/sourcecode]

Il faut ensuite modifier son fichier ``~/.bashrc`` pour y déclarer les
bonnes variables d'environnement, en particulier ``$MSYS_HOME``
nécéssaire au fonctionnement de VirtualEnvWrapper sous MSYS:

[sourcecode language="bash" light="true" title="~/.bashrc"]

# VirtualEnv et VirtualEnvWrapper

export PYTHON\_HOME=/c/Python27

export WORKON\_HOME=$HOME/virtualenvs

export MSYS\_HOME=/c/msysgit/

export PIP\_VIRTUALENV\_BASE=$WORKON\_HOME

export PIP\_RESPECT\_VIRTUALENV=true

source $PYTHON\_HOME/Scripts/virtualenvwrapper.sh

[/sourcecode]

Il est alors possible d'effectuer les opérations décrite `ici`_.

.. _ici: http://sourceforge.net/projects/pywin32/files/
.. _ici: http://pypi.python.org/pypi/setuptools#files
.. _setuptools-0.6c11.win32-py2.7.exe: http://pypi.python.org/packages/2.7/s/setuptools/setuptools-0.6c11.win32-py2.7.exe#md5=57e1e64f6b7c7f1d2eddfc9746bbaf20
.. _ici: http://noirbizarre.info/2010/07/10/pip-virtualenv-et-pydev/
