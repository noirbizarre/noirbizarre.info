Git: Smart HTTP
###############
:date: 2010-12-11 11:30
:author: Noirbizarre
:category: Non classé

Nous allons rendre créer des dépôts git répondants aux critères
suivants:

-  accessible via le `protocol Smart HTTP`_ pour les clients git
-  accessible via `gitweb`_ pour les browsers
-  uniquement accessible depuis https
-  utilise l'authentification via `PAM`_
-  possibilité de gérer:

   .. raw:: html

      </p>

   -  lecture seule pour tous
   -  lecture seule en anonyme et écriture pour les utilisateurs
      authentifiés
   -  lecture et écriture pour tous les utilisateurs authentifiés
   -  lecture pour tous les utilisateurs authentifiés et écriture
      autorisée pour certains seulement

   .. raw:: html

      </p>

   .. raw:: html

      <p>

-  Accessible depuis le domain git.noirbizarre.info

.. raw:: html

   </p>

Génération du certificat
------------------------

.. raw:: html

   </p>

Pour cet exemple, nous allons utilisé un certificat autosigné. Il permet
de crypter la communication mais le fait qu’il soit auto signé ne permet
d’authentifier l’hôte distant. OpenSSL doit être installé pour cette
étape. En répondant aux questions, on s’assurera que Common Name est
bien rempli avec le nom de domaine (Cherokee s’en sert pour le routage).

.. raw:: html

   <p>

::

    ~$ openssl req -new -x509 -nodes -out git.crt -keyout git.key -days 3650Generating a 1024 bit RSA private key...............................................++++++.++++++writing new private key to 'git.key'-----You are about to be asked to enter information that will be incorporated into your certificate request.What you are about to enter is what is called a Distinguished Name or a DN.There are quite a few fields but you can leave some blankFor some fields there will be a default value,If you enter '.', the field will be left blank.-----Country Name (2 letter code) [AU]:FRState or Province Name (full name) [Some-State]:FranceLocality Name (eg, city) []:Organization Name (eg, company) [Internet Widgits Pty Ltd]:noirbizarre.infoOrganizational Unit Name (eg, section) []:GIT RepositoryCommon Name (eg, YOUR name) []:git.noirbizarre.infoEmail Address []:~$ mv git.crt git.key /var/www/noirbizarre.info

.. raw:: html

   </p>

Création du système de fichier
------------------------------

.. raw:: html

   </p>

L'ensemble du dépôt sera stocké dans */var/www/noirbizarre.info/git*.

Création du vServer dans Cherokee
---------------------------------

.. raw:: html

   </p>

Nous allons créer le vServer git.noirbizarre.info dans Cherokee et le
rendre accessible uniquement depuis l'accès https.

Règles de gestion
~~~~~~~~~~~~~~~~~

.. raw:: html

   </p>

﻿sudo aptitude install

.. raw:: html

   <p>

::

    (?x)^/(.*/(HEAD | \                        info/refs | \                        objects/(info/[^/]+ | \                                 [0-9a-f]{2}/[0-9a-f]{38} | \                                 pack/pack-[0-9a-f]{40}\.(pack|idx)) | \                        git-(upload|receive)-pack))$

.. raw:: html

   </p>

.. _protocol Smart HTTP: http://progit.org/2010/03/04/smart-http.html
.. _gitweb: https://git.wiki.kernel.org/index.php/Gitweb
.. _PAM: http://fr.wikipedia.org/wiki/Pluggable_Authentication_Modules
