Utilisation de distribute
#########################
:date: 2010-11-27 18:14
:author: Noirbizarre
:category: Développement, Mémentos, Python
:tags: distribute, python, setuptools

`Distribute`_ est un fork de `setuptools`_ qui corrige certains de ses
défauts. Il est voué à le remplacer et il est recommandé de l'utilisé à
la place de setuptools.

Cette article s'appuie sur la `documentation officielle de Distribute`_.

Initialisation du projet
------------------------

.. raw:: html

   </p>

Distribute étant un fork de setuptools, il s'utilise de la même façon:
on crée un fichier **setup.py** dans lequel on va réaliser tout le
travail de packaging.

Setuptools étant installé par défaut sur la plupart des plateformes à
l'inverse de Distribute, on va forcer son installation depuis le fichier
setup.py. Pour cela, nous allons télécharger un module d'initialisation
de distribute et l'invoquer depuis notre setup.py.

Le module est disponible ici:
`http://python-distribute.org/distribute\_setup.py`_ et doit être placé
dans le même répertoire que setup.py. Il faut ensuite appeler la
fonction **use\_setuptools()** qui va forcer l'installation de
distribute.

Pour que notre projet soit indexable par `Python Package Index (PyPI)`_,
il faut au minimum fournir le nom et la version de l'application, ainsi
que la liste ses packages.

L'aborescence de notre projet est la suivante:

.. raw:: html

   <p>

::

    myproject/|-- bin/|   `-- myapp.py|-- myapp/|   |-- subpackage/|   |   `-- __init__.py|   |-- __init__.py|   |-- utils.py|   `-- other.py||-- data/|   |-- xml/|   |   |-- file1.xml|   |   `-- file2.xml|   `-- icons/|       `-- myapp.png||-- mymod.py|-- setup.py|-- distribute_setup.py|-- README`-- LICENSE

.. raw:: html

   </p>

Notre fichier setup.py doit donc ressembler à ceci:

.. raw:: html

   <p>

::

    # Force l'installation de distributefrom distribute_setup import use_setuptoolsuse_setuptools()# Importe les outils fourni par distributefrom setuptools import setupsetup(    name = 'Mon appli',    version = '0.1',    packages = ['myapp', 'myapp.subpackage'],)

.. raw:: html

   </p>

Nous avons donc la version minimale de notre fichier setup.py. Au lieu
de lister manuellement les packages, nous pouvons utiliser le helper
**find\_package() qui va parcourir de façon récursive l'arborescence de
notre projet pour créer la liste des packages.**

.. raw:: html

   <p>

::

    # Force l'installation de distributefrom distribute_setup import use_setuptoolsuse_setuptools()# Importe les outils fourni par distributefrom setuptools import setup, find_packagessetup(    name = 'Mon appli',    version = '0.1',    packages = find_packages(),)

.. raw:: html

   </p>

Point important, la version doit suivre une spécification particulière
décrite `ici`_.

Description du projet
---------------------

.. raw:: html

   </p>

Nous allons enrichir la description de notre projet avec notre nom,
notre email, une courte description du projet, une description
détaillée, la license, une page d'accueil et un lien de téléchargement.

.. raw:: html

   <p>

::

    # Force l'installation de distributefrom distribute_setup import use_setuptoolsuse_setuptools()# Importe les outils fourni par distributefrom setuptools import setup, find_packagessetup(    name = 'Mon appli',    version = '0.1',    author = 'Moi',    author_email = 'moi@mail.com',    description = 'An usefull application',    long_description = 'This application does something and it does it so well you can't live without!!!',    license = 'GNU GPL',    url = 'http://www.myapp.org',    download_url = 'http://www.myapp.org/myapp.tar.gz',    packages = find_packages(),)

.. raw:: html

   </p>

Toutes ces informations apparaîtrons sur la page PyPI de votre
application, il est donc important d'être clair et précis.

Gestion des fichiers de données
-------------------------------

.. raw:: html

   </p>

Par défait, distribute ne va inclure les fichiers sources python
lorsqu'il va parcourir les packages. Il est possible d'inclure de type
de données en plus: les fichiers de données situés dans les packages qui
seront recopiés en même temps que les fichiers sources et les fichiers
de données hors packages que l'on recopiera dans les répertoires que
l'on choisira.

Si l'on veut recopier tous les fichiers de données inclus dans les
packages, sans distinctions, il suffit de rajouter l'option suivante
dans les paramètres de setup():

.. raw:: html

   <p>

::

    include_package_data = True

.. raw:: html

   </p>

Gestion des dépendances
-----------------------

.. raw:: html

   </p>

Information officielle sur PIP et Distribute
--------------------------------------------

.. raw:: html

   </p>

Voici une petit image d'information que l'on peut trouver sur PIP et
Distribute:

.. figure:: http://python-distribute.org/pip_distribute.png
   :align: center
   :alt: Distribute

   Distribute et PIP à la place de setuptools et easy\_install

.. raw:: html

   </p>

.. _Distribute: http://pypi.python.org/pypi/distribute
.. _setuptools: http://pypi.python.org/pypi/setuptools
.. _documentation officielle de
Distribute: http://packages.python.org/distribute/
.. _`http://python-distribute.org/distribute\_setup.py`: http://python-distribute.org/distribute_setup.py
.. _Python Package Index (PyPI): http://pypi.python.org/
.. _ici: http://packages.python.org/distribute/setuptools.html#specifying-your-project-s-version
