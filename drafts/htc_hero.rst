HTC Hero: root et ROM alternatives
##################################
:date: 2010-01-03 21:48
:author: Noirbizarre
:category: Hardware

Après 6 mois d'utilisation de mon HTC Hero, le bilan est plutôt mitigé:

-  Lenteurs et blocages intempestifs allant jusqu'au plantage en cours
   de communication voir impossibilité de décrocher.
-  Certaines applications sont modifiées par Orange pour faire du
   hors-forfait (ex: gmail fait de l'imap donc du hors-forfait).
-  Une mise à jour annoncée vers Android 1.6, puis 2.0, puis 2.1 sans
   cesse reculée depuis 1 an (mon prochaine ne sera probablement pas un
   HTC!!)
-  Bluetooth partiellement implémenté.
-  Consommation électrique du firmware radio trop élevée.
-  Tous les autres inconvénients de Android 1.5 par rapport à Android
   2.1: le téléphone donne l'impression d'être en beta-test.

.. raw:: html

   </p>

C'est bien dommage car le téléphone est très bien fini et la surcouche
HTC Sense pourrait faire la différence face à un iPhone ou un motorola
Droid/Milestone.

Suite à un n-ième recul de la date de mise à jour HTC, je décide
finalement de passer sur une ROM alternatives en 2.1. Plusieurs choix
s'offrent à moi aujourd'hui et je retiens les ROMs suivantes:

-  `VillainROM 5.3`_
-  `BeHero 1.3.2 Legendary Edition`_
-  `SenseHero 2.1 v2`_

.. raw:: html

   </p>

Après quelques recherches sur le web, voici mon cheminement.

Installation d'une recovery ROM alternative
-------------------------------------------

.. raw:: html

   </p>

Nous allons installer `AmonRa Recovery`_ en utilisant `FlashRec`_.

.. _VillainROM 5.3: http://www.villainrom.co.uk/viewtopic.php?f=23&t=200
.. _BeHero 1.3.2 Legendary
Edition: http://htcpedia.com/forum/showthread.php?t=2196
.. _SenseHero 2.1
v2: http://www.htcdevscene.info/showthread.php?63-[ROM]-[24-04-10]-SenseHero-2-1-v2-Beauty-Speed-Stable-[ONLINE]
.. _AmonRa
Recovery: http://forum.xda-developers.com/showthread.php?t=561124
.. _FlashRec: http://zenthought.org/content/project/flashrec
