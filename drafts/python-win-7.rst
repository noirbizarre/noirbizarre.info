Environnement de développement Python complet sous Windows 7
############################################################
:date: 2012-08-31 14:29
:author: Noirbizarre
:category: Python

http://www.lfd.uci.edu/~gohlke/pythonlibs/

Python 2.7

Distribute

pip

mingw-get install msys-coreutils

mingw-get install msys-mktemp

mingw-get install mingw32-gendef

mingw-get install msys-man

patch distutils -mno-cygwin

patch

mingw-get install msys-mktemp

.bashrc

path pour mingw64

alias python

conf virtualenvwrapper

Thanks, it worked!

Maybe this summary can be useful for others:

Step by step recipe to compile 64-bit cython extensions with python

2.6.6 with mingw compiler in win 7 64-bit

\*\*Install mingw compiler\*\*

1) Install tdm64-gcc-4.5.2.exe for 64-bit compilation

\*\*Apply patch to python.h\*\*

2) Modify python.h in C:\\python26\\include as indicated in

http://bugs.python.org/file12411/mingw-w64.patch

\*\*Modify distutils\*\*

3) Eliminate all the parameters -mno-cygwin fom the call to gcc in the

Mingw32CCompiler class in Python26\\Lib\\distutils\\cygwinccompiler.py

4) In the same module, modify get\_msvcr() to return an empty list

instead of ['msvcr90'] when msc\_ver == '1500' .

\*\*Produce the libpython.a file\*\*

5) Obtain gendef.exe from mingw-w64-bin\_x86\_64-

mingw\_20101003\_sezero.zip

(gendef.exe is not available in the tmd64 distribution. Another

solution is to compile gendef from source...)

6) Copy python26.dll (located at C\\windows\\system32) to the user

directory (C:\\Users\\myname)

7) Produce the python26.def file with:

> gendef.exe C:\\Users\\myname\\python26.dll

8) Move the python.def file produced (located in the folder from where

gendef was executed) to the user directory

9) Produce the libpython.a with:

> dlltool -v --dllname python26.dll --def C:\\Users\\myname

\\python26.def --output-lib C:\\Users\\myname\\libpython26.a

10) Move the created libpython26.a to C:\\Python26\\libs

\*\*Produce your .pyd extension\*\*

11) Create a test hello.pyx file and a setup.py file as indicated in

cython tutorial (http://docs.cython.org/src/quickstart/build.html)

12) Compile with

> python setup.py build\_ext --inplace

Done!

https://groups.google.com/d/msg/cython-users/r\_0A5AHwiak/\_pimKqH1beQJ
