AirORM
######
:date: 2010-08-15 12:16
:author: Noirbizarre

AirORM est un ORM de type ActiveRecord pour Adobe Flex/Air.

Il repose sur les annotations.

AirORM est disponible ici: `http://airorm.googlecode.com`_

.. _`http://airorm.googlecode.com`: http://airorm.googlecode.com
