Git illustré
############
:date: 2012-03-31 16:24
:author: Noirbizarre
:category: Développement
:tags: git

Git est un outil extrêmement puissant mais il nécessite de bien
comprendre son fonctionnement. Rien ne vaut de bons schémas pour
illustrer son fonctionnement.

[svg src="/wp-content/uploads/2012/03/git-base.svg" height="300"
type="embed"]
