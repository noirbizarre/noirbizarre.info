Affaire Wikileaks: vers un nouvel ordre de l'Internet ?
#######################################################
:date: 2010-12-12 04:49
:author: Noirbizarre
:category: Actualité

Aujourd'hui on ne peut passer passer à côté de cette affaire médiatique
qui fait frémir les gouvernements: `Wikileaks`_. Ce tolé diplomatique
remet au premier plan des débats de fond sur l'architecture et le
fonctionnement d'Internet, débats qui, auraient eu lieu un jour ou
l'autre.

Guerre pour le contrôle de l'Internet
-------------------------------------

.. raw:: html

   </p>

Une guerre pour le contrôle de l'Internet est en train de se livrer, et
ce sur plusieurs terrains. Les états veulent contrôler l'information et
les grands groupes veulent centraliser les usages. Ce débat concerne
tout le monde puisque ce sont avant tout nos données, notre espace et
nos usages.

Contrôle de l'accès
~~~~~~~~~~~~~~~~~~~

.. raw:: html

   </p>

Les états souhaiteraient contrôler l'accès aux sources d'informations,
sélectionner les sites auxquels nous pouvons accéder, un peu comme si
l'état sélectionnait les livres que nous avons le droit de lire.

Pour cela, ils ont plusieurs levier sur lesquels ils peuvent

`http://dot-p2p.org`_

`http://www.icann.org/`_

Contrôle des données
~~~~~~~~~~~~~~~~~~~~

.. raw:: html

   </p>

La nuance est subtile mais contrôler les données n'es pas la même chose
que contrôler l'accès. Pour reprendre le parallèle avec la lecture, ce
serait comme si les états pouvait sélectionner les pages des livres que
nous pouvons lire. L'objectif secret souhaité des états serait de
pouvoir appliquer le modèle Chinois.

Contrôle des échanges
~~~~~~~~~~~~~~~~~~~~~

.. raw:: html

   </p>

Les alternatives possibles
--------------------------

.. raw:: html

   </p>

Tous les recours et contournements possible se résumé à la seule
expression de "peer to peer" généralement abrégée P2P. Même si les
grandes instances s'évertuent à contrôler nos échanges de pair à pair,
il n'y a aucune chance qu'elles puissent un jour nous les bloquer.
Malheureusement pour elles, c'est le modèle vers lequel tout l'internet
tend.

cryptage

p2p-dns

.. _Wikileaks: http://wikileaks.ch
.. _`http://dot-p2p.org`: http://dot-p2p.org/
.. _`http://www.icann.org/`: http://www.icann.org/
