==================================================================
https://keybase.io/noirbizarre
--------------------------------------------------------------------

I hereby claim:

  * I am an admin of https://noirbizarre.info
  * I am noirbizarre (https://keybase.io/noirbizarre) on keybase.
  * I have a public key ASA8nYmizKcgsltQDyGN0SKMUQRUaIkjsQbRjun--hFrmAo

To do so, I am signing this object:

{
  "body": {
    "key": {
      "eldest_kid": "012049011eeb647b9963b0fb1b873774596a1451eb046767caa21758379cddc4eccc0a",
      "host": "keybase.io",
      "kid": "01203c9d89a2cca720b25b500f218dd1228c510454688923b106d18ee9fefa116b980a",
      "uid": "6505785f770739ce9c643b767a559919",
      "username": "noirbizarre"
    },
    "merkle_root": {
      "ctime": 1516112556,
      "hash": "c87cd8e4e5959fba9d8c16969d1a8ffc26da430885df59687d92c70a6493150af2431368ef8e753dce30c712b5d7b661a96c5b0b9df4746f8228bc7d976d38af",
      "hash_meta": "4d6833a27a23abcaa7f53e8b92cbcc4206ffe0a62aacd26213a96ffc29d865f3",
      "seqno": 1932503
    },
    "service": {
      "hostname": "noirbizarre.info",
      "protocol": "https:"
    },
    "type": "web_service_binding",
    "version": 1
  },
  "client": {
    "name": "keybase.io go client",
    "version": "1.0.39"
  },
  "ctime": 1516112591,
  "expire_in": 504576000,
  "prev": "4ff37c2d0c54d2d77d98405f88a9ae03698682ba82f098952cd76404d16ea99e",
  "seqno": 23,
  "tag": "signature"
}

which yields the signature:

hKRib2R5hqhkZXRhY2hlZMOpaGFzaF90eXBlCqNrZXnEIwEgPJ2JosynILJbUA8hjdEijFEEVGiJI7EG0Y7p/voRa5gKp3BheWxvYWTFA0x7ImJvZHkiOnsia2V5Ijp7ImVsZGVzdF9raWQiOiIwMTIwNDkwMTFlZWI2NDdiOTk2M2IwZmIxYjg3Mzc3NDU5NmExNDUxZWIwNDY3NjdjYWEyMTc1ODM3OWNkZGM0ZWNjYzBhIiwiaG9zdCI6ImtleWJhc2UuaW8iLCJraWQiOiIwMTIwM2M5ZDg5YTJjY2E3MjBiMjViNTAwZjIxOGRkMTIyOGM1MTA0NTQ2ODg5MjNiMTA2ZDE4ZWU5ZmVmYTExNmI5ODBhIiwidWlkIjoiNjUwNTc4NWY3NzA3MzljZTljNjQzYjc2N2E1NTk5MTkiLCJ1c2VybmFtZSI6Im5vaXJiaXphcnJlIn0sIm1lcmtsZV9yb290Ijp7ImN0aW1lIjoxNTE2MTEyNTU2LCJoYXNoIjoiYzg3Y2Q4ZTRlNTk1OWZiYTlkOGMxNjk2OWQxYThmZmMyNmRhNDMwODg1ZGY1OTY4N2Q5MmM3MGE2NDkzMTUwYWYyNDMxMzY4ZWY4ZTc1M2RjZTMwYzcxMmI1ZDdiNjYxYTk2YzViMGI5ZGY0NzQ2ZjgyMjhiYzdkOTc2ZDM4YWYiLCJoYXNoX21ldGEiOiI0ZDY4MzNhMjdhMjNhYmNhYTdmNTNlOGI5MmNiY2M0MjA2ZmZlMGE2MmFhY2QyNjIxM2E5NmZmYzI5ZDg2NWYzIiwic2Vxbm8iOjE5MzI1MDN9LCJzZXJ2aWNlIjp7Imhvc3RuYW1lIjoibm9pcmJpemFycmUuaW5mbyIsInByb3RvY29sIjoiaHR0cHM6In0sInR5cGUiOiJ3ZWJfc2VydmljZV9iaW5kaW5nIiwidmVyc2lvbiI6MX0sImNsaWVudCI6eyJuYW1lIjoia2V5YmFzZS5pbyBnbyBjbGllbnQiLCJ2ZXJzaW9uIjoiMS4wLjM5In0sImN0aW1lIjoxNTE2MTEyNTkxLCJleHBpcmVfaW4iOjUwNDU3NjAwMCwicHJldiI6IjRmZjM3YzJkMGM1NGQyZDc3ZDk4NDA1Zjg4YTlhZTAzNjk4NjgyYmE4MmYwOTg5NTJjZDc2NDA0ZDE2ZWE5OWUiLCJzZXFubyI6MjMsInRhZyI6InNpZ25hdHVyZSJ9o3NpZ8RATr4Y9ze9ENxuHvrwxvxVM490tMzYB9dHqPAJjsLAw+8op864KZZEfbCAqlZW0aO84MDTjOUgBi1B6DRZW0GKBqhzaWdfdHlwZSCkaGFzaIKkdHlwZQildmFsdWXEIAbPN09LUZV4vMLddNcUZzlB3sGBZgNCl9gWXzmFN9+Zo3RhZ80CAqd2ZXJzaW9uAQ==

And finally, I am proving ownership of this host by posting or
appending to this document.

View my publicly-auditable identity here: https://keybase.io/noirbizarre

==================================================================
